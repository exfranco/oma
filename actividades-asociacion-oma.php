<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner">				
                <div class="container container_asociacion">
                    <div class="box">
                        <img src="images/banner-asociacion-oma.png" style="max-width:1196px;" alt="Asociacion Oma">
                    </div>
                    <div class="btn_donaaqui btn_blanco">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">Actividades</a></li>
                            <li>/ <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="activo">  Asociación OMA</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3  order-md-1">
                                <div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral sub_menu">
                                    <li>
                                        <a href="que-hacemos-actividades.php" class="full"></a>
                                        Actividades
                                    </li>
                                    <li>
                                        <a href="actividades-programa-mujeres-roca.php" class="full"></a>
                                        Programa Mujeres Roca
                                    </li>
                                    <li>
                                        <a href="actividades-programa-de-coaching.php" class="full"></a>
                                        Programa de Coaching
                                    </li>
                                    <li class="activo">
                                        <a href="actividades-asociacion-oma.php" class="full"></a>
                                        Asociación OMA
                                    </li>
                                    <li>
                                        <a href="actividades-mentoring.php" class="full"></a>
                                        Mentoring
                                    </li>
                                    <li>
                                        <a href="actividades-curso-de-quechua.php" class="full"></a>
                                        Curso de Quechua
                                    </li>
                                    <li>
                                        <a href="actividades-congreso-oma.php" class="full"></a>
                                        Congreso OMA
                                    </li>
                                    <li>
                                        <a href="actividades-ponencias-para-comunidad-oma.php" class="full"></a>
                                        Ponencias para Comunidad Oma
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="full"></a>
                                        VOLUNTARIADO EN PROYECTOS DE DESARROLLO SOSTENIBLE Y RESPONSABILIDAD SOCIAL
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-de-la-comunidad-oma.php" class="full"></a>
                                        VOLUNTARIADO DE LA COMUNIDAD OMA
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2 class="h2_naranja">
                                      Asociación OMA
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="texto">
                                    <p><span class="orange">Ser socio te permitirá:</span><br>
                                    Postular a las becas y medias becas de inglés, Data Science, Cetemin, así como postular al mentoring internacional de la Escuela de Mentoring de España, clases de quechua, becas para sesiones individuales de coaching, bolsa de trabajo, entre otras.
                                </div>
                                <div class="lista_texto_imagen">
                                    <article>
                                        <div class="texto">
                                            <span class="mayor">
                                                Bolsa de Trabajo
                                            </span> <br><br>
                                            Donde podrás encontrar oportunidades laborales de nuestras empresas aliadas brindadas de primera mano, permitiéndote así posicionarte en los mejores cargos del sector minero
                                        </div>
                                        <!-- <div class="btn_vermas">
                                            <a href="bolsa-de-trabajo.php" class="full"></a>
                                            Ver Más
                                        </div> -->
                                        <div class="imagen">
                                            <img src="images/asociacion-oma-1.jpg">
                                        </div>
                                    </article>

                                    <article>
                                        <div class="texto">
                                            <span class="mayor">
                                                Becas
                                            </span> <br><br>
                                            Brindadas por nuestros aliados estratégicos que te ayudarán a seguir desarrollándote tanto profesional como personalmente es distintos temas ligados al sector <br>
                                            <ul class="lista_check_orange my-0">
                                                <li>12 becas de la Escuela de Mentoring de España</li>
                                                <li>15 becas de Data Science: Introducción a la ciencia de datos</li>
                                                <li>20 medias becas de CETEMIN, único Instituto licenciado por el Minedu con carreras técnicas de 11 meses.</li>
                                                <li>5  medias becas de inglés del Instituto Peruano Norteamericano </li>
                                                <li>Maestrias online, pasantías de la Universidad de Celaya. (México y EEUU)</li>
                                            </ul>

                                        </div>
                                        <div class="btn_vermas">
                                            <a href="becas.php" class="full"></a>
                                            Ver Más
                                        </div>
                                        <div class="imagen">
                                            <img src="images/asociacion-oma-2.jpg">
                                        </div>
                                    </article>
                                    <article>
                                        <div class="texto">
                                            <span class="mayor">
                                                Laboratorios
                                            </span> <br><br>
                                            Espacios de aprendizaje y experimentación grupal, en los que cada participante lleva a cabo una evaluación a 360°, aprendiendo estrategias y técnicas precisas de desarrollo humano y competencias blandas, finalmente, evalúa el impacto de la metodología aplicada en tiempo real. <br>
                                            Cada laboratorio es diseñado de acuerdo con las necesidades específicas de la entidad que lo solicita. <br>
                                            La duracion es de 16 horas en forma presencial y 10 horas de manera virtual. 
                                        </div>
                                        <div class="btn_vermas">
                                            <a href="laboratorio.php" class="full"></a>
                                            Ver Más
                                        </div>
                                        <div class="imagen">
                                            <img src="images/asociacion-oma-3.jpg">
                                        </div>
                                    </article>
                                    <article>
                                        <div class="texto">
                                            <span class="mayor">
                                                Mentoring
                                            </span> <br><br>
                                            Talleres grupales de mentoring que permiten a los estudiantes estar en contacto con empresarios de trayectoria y reconocido prestigio que comparten sus experiencias de vida y los motivan a alcanzar sus metas. <br>
                                            Sesiones de mentoring  individual pro bono con mentores nacionales e internacionales bajo la metodología de la Escuela de Mentoring de España.
                                        </div>
                                        <div class="btn_vermas">
                                            <a href="mentoring.php" class="full"></a>
                                            Ver Más
                                        </div>
                                        <div class="imagen">
                                            <img src="images/asociacion-oma-4.jpg">
                                        </div>
                                    </article>
                                    <article>
                                        <div class="texto">
                                            <span class="mayor">
                                                Programa de Coaching
                                            </span> <br><br>
                                            Sesiones grupales de aprendizaje teórico práctico sobre objetivos puntuales de competencias blandas y metodologías que aportan al desarrollo humano enfocado en el campo profesional, empleabilidad, responsabilidad social, entre otros temas relevantes. <br>
                                            Cada taller de coaching es diseñado de acuerdo con las necesidades específicas de la entidad que lo solicita. La duración de cada taller es de 4 horas de forma presencial y 2 horas con 30 minutos de manera virtual.

                                        </div>
                                        <div class="btn_vermas">
                                            <a href="actividades-programa-de-coaching.php" class="full"></a>
                                            Ver Más
                                        </div>
                                        <div class="imagen">
                                            <img src="images/asociacion-oma-5.jpg">
                                        </div>
                                    </article>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>

                

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>
                
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>