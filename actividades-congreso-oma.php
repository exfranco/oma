<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner" style="background-image: url(images/banner-congreso-oma.jpg);">				
                <div class="container">
                    <div class="box">                        
                        <h1>
                            III CONGRESO OMA
                        </h1>
                        <div class="subtitulo">
                            Evento que convoca a estudiantes de todas las regiones del <br>
                            país y de algunos países de Latinoamérica. 
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">Actividades</a></li>
                            <li>/ <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="activo">Congreso OMA</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3  order-md-1">
                                <div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral sub_menu">
                                    <li>
                                        <a href="que-hacemos-actividades.php" class="full"></a>
                                        Actividades
                                    </li>
                                    <li>
                                        <a href="actividades-programa-mujeres-roca.php" class="full"></a>
                                        Programa Mujeres Roca
                                    </li>
                                    <li>
                                        <a href="actividades-programa-de-coaching.php" class="full"></a>
                                        Programa de Coaching
                                    </li>
                                    <li>
                                        <a href="actividades-asociacion-oma.php" class="full"></a>
                                        Asociación OMA
                                    </li>
                                    <li>
                                        <a href="actividades-mentoring.php" class="full"></a>
                                        Mentoring
                                    </li>
                                    <li>
                                        <a href="actividades-curso-de-quechua.php" class="full"></a>
                                        Curso de Quechua
                                    </li>
                                    <li class="activo">
                                        <a href="actividades-congreso-oma.php" class="full"></a>
                                        Congreso OMA
                                    </li>
                                    <li>
                                        <a href="actividades-ponencias-para-comunidad-oma.php" class="full"></a>
                                        Ponencias para Comunidad Oma
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="full"></a>
                                        VOLUNTARIADO EN PROYECTOS DE DESARROLLO SOSTENIBLE Y RESPONSABILIDAD SOCIAL
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-de-la-comunidad-oma.php" class="full"></a>
                                        VOLUNTARIADO DE LA COMUNIDAD OMA
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2 class="h2_naranja">
                                    III Congreso OMA
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="texto">
                                    <p>El Congreso OMA, que se realiza cada dos años, convoca a un gran número de estudiantes destacados de todo el país y de la región latinoamericana. Comprende 5 días de intenso aprendizaje que incluyen talleres de coaching y emprendimiento, visita técnica, sesiones de mentoring con destacados profesionales y conferencias del Foro Educativo en el marco de la feria Expomina Perú.  </p>
                                    <p>Los días de convivencia, talleres y participación en Expomina, les permite tener una visión integral del sector y de las oportunidades que la minería ofrece para el desarrollo sostenible del país.</p>
                                    <p>Asimismo, apoyamos y participamos en los encuentros del Congreso Internacional de Estudiantes de Minería – CIEMIN, y el Encuentro de Amautas Mineros que se organizan en el marco de Perumin cada dos años.</p>
                                </div>

                                <article class="lista_congreso">
                                    <div class="tit">
                                        2 Días de Laboratorios de Coaching
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img src="images/congreso-oma-1.jpg">
                                        </div>
                                        <div class="col-md-4">
                                            <img src="images/congreso-oma-2.jpg">
                                        </div>
                                    </div>
                                </article>

                                <article class="lista_congreso">
                                    <div class="tit">
                                        1 DÍA DE VISITA A LA MINA
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img src="images/congreso-oma-3.jpg">
                                        </div>
                                        <div class="col-md-4">
                                            <img src="images/congreso-oma-4.jpg">
                                        </div>
                                    </div>
                                </article>

                                <article class="lista_congreso">
                                    <div class="tit">
                                        1 DÍA DE MENTORIAS     
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img src="images/congreso-oma-5.jpg">
                                        </div>
                                        <div class="col-md-4">
                                            <img src="images/congreso-oma-6.jpg">
                                        </div>
                                    </div>
                                </article>

                                <article class="lista_congreso">
                                    <div class="tit">
                                        1 DÍA DE CHARLAS Y MENTORIAS EN LA FERIA DE EXPOMINA
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img src="images/congreso-oma-7.jpg">
                                        </div>
                                        <div class="col-md-4">
                                            <img src="images/congreso-oma-8.jpg">
                                        </div>
                                    </div>
                                </article>

                                <br><br><br><br>

                                
                                
                            </div>                            
                        </div>
                    </div>
                </div>
               

                <?php include 'seccion_testimonios.html';?>

                <?php include 'seccion-nuestro-blog.html';?>


            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>