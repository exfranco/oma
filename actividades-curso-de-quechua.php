 <!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal" >
			<div class="banner"  style="background-image: url(images/banner-curso-de-quechua.jpg); background-position:top;">				
                <div class="container">
                    <div class="box">
                        <h1>
                            CLASES DE QUECHUA <br>
                            NO PRESENCIAL
                        </h1>
                        <div class="subtitulo">
                            Con el objetivo de cubrir las necesidades básicas de <br>
                            comunicación para que, desde una óptica cultural  e histórica, <br>
                            el participante pueda acercarse a las costumbres y tradiciones <br>
                            de las regiones quechua hablantes.
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">Actividades</a></li>
                            <li>/ <a href="actividades-curso-de-quechua.php" class="activo">Curso de Quechua</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">                            
                            <div class="col-md-3 order-md-1"><div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral sub_menu">
                                    <li>
                                        <a href="que-hacemos-actividades.php" class="full"></a>
                                        Actividades
                                    </li>
                                    <li>
                                        <a href="actividades-programa-mujeres-roca.php" class="full"></a>
                                        Programa Mujeres Roca
                                    </li>
                                    <li>
                                        <a href="actividades-programa-de-coaching.php" class="full"></a>
                                        Programa de Coaching
                                    </li>
                                    <li>
                                        <a href="actividades-asociacion-oma.php" class="full"></a>
                                        Asociación OMA
                                    </li>
                                    <li>
                                        <a href="actividades-mentoring.php" class="full"></a>
                                        Mentoring
                                    </li>
                                    <li class="activo">
                                        <a href="actividades-curso-de-quechua.php" class="full"></a>
                                        Curso de Quechua
                                    </li>
                                    <li>
                                        <a href="actividades-congreso-oma.php" class="full"></a>
                                        Congreso OMA
                                    </li>
                                    <li>
                                        <a href="actividades-ponencias-para-comunidad-oma.php" class="full"></a>
                                        Ponencias para Comunidad Oma
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="full"></a>
                                        VOLUNTARIADO EN PROYECTOS DE DESARROLLO SOSTENIBLE Y RESPONSABILIDAD SOCIAL
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-de-la-comunidad-oma.php" class="full"></a>
                                        VOLUNTARIADO DE LA COMUNIDAD OMA
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2 class="h2_naranja">
                                    Curso de Quechua
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="texto">

                                    <p>
                                        El quechua es una lengua hablada por más de ocho millones de latinoamericanos y al menos por cuatro millones de peruanos en sus diversas variedades. Aprende Quechua con nuestra modalidad especial no presencial.
                                    </p>

                                </div>
                                <ul class="lista_acordion">
                                    <li>
                                        <div class="titulo">
                                            Objetivo
                                        </div>
                                        <div class="desplegable">
                                            <div class="texto">
                                                Cubrir las necesidades básicas de comunicación para que, desde una óptica cultural e histórica, el participante pueda acercarse a las costumbres y tradiciones de las regiones quechua hablantes. Dirigido al público en general.
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="titulo">
                                            METODOLOGÍA DE ENSEÑANZA
                                        </div>
                                        <div class="desplegable">
                                            <div class="texto">
                                                <p>Gira en torno a la realidad cultural y a los intereses de los participantes. Se caracteriza por ser vivencial, comunicativa, activa, lúdica y descubrimiento guiado.</p>

                                                <p>
                                                    Talleres vivenciales <br>
                                                    Acompañamiento permanente <br>
                                                    Enfoque cultural (costumbres, tradiciones, valores, cosmovisión andina, modos de ver el mundo) <br>
                                                    Uso de materiales didácticos.
                                                </p>

                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="titulo">
                                            ORGANIZACIÓN
                                        </div>
                                        <div class="desplegable">
                                            <div class="texto">
                                                <p>
                                                    Niveles:  KUSA  <br>
                                                    Cantidad de ciclos por nivel: 4 unidades <br>
                                                    Frecuencias: Interdiaria (Lu-Mi / Ma-Ju)
                                                </p>

                                                <p>
                                                    Niveles:  Kuskalla <br>
                                                    Cantidad de ciclos por nivel: 4 unidades <br>
                                                    Frecuencias: Interdiaria (Lu-Mi / Ma-Ju)
                                                </p>


                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="titulo">
                                            CALENDARIO E INVERSIÓN
                                        </div>
                                        <div class="desplegable">
                                            <div class="texto">
                                                Próximos inicios 16 y 17 de mayo:

                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <table border="0" celpadding="0" celspacing="0">
                                                                <tr>
                                                                    <td>Frecuencia: </td>
                                                                    <td>(Lun-Mie)</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Horas pedagógicas: </td>
                                                                    <td>4 horas semanales</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Duración por ciclo: </td>
                                                                    <td>1 mes</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Inicio: </td>
                                                                    <td>16- may</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Costo por ciclo:  </td>
                                                                    <td>S/ 80.00</td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <table border="0" celpadding="0" celspacing="0">
                                                                <tr>
                                                                    <td>Frecuencia:   </td>
                                                                    <td>(Lun-Mie)</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Horas pedagógicas:</td>
                                                                    <td>4 horas semanales</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Duración por ciclo: </td>
                                                                    <td>1 mes</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Inicio: </td>
                                                                    <td>20 - may</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Costo por ciclo: </td>
                                                                    <td>S/ 80.00</td>
                                                                </tr>

                                                            </table>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <table border="0" celpadding="0" celspacing="0">
                                                                <tr>
                                                                    <td>Frecuencia:</td>
                                                                    <td>(Lun-Mie)</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Horas pedagógicas:</td>
                                                                    <td>4 horas semanales</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Duración por ciclo:</td>
                                                                    <td>1 mes</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Inicio:</td>
                                                                    <td>25 - may</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Costo por ciclo:</td>
                                                                    <td>S/ 80.00</td>
                                                                </tr>

                                                            </table>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <table border="0" celpadding="0" celspacing="0">
                                                                <tr>
                                                                    <td>Frecuencia:</td>
                                                                    <td>(Lun-Mie)</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Horas pedagógicas:</td>
                                                                    <td>4 horas semanales</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Duración por ciclo:</td>
                                                                    <td>1 mes</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Inicio:</td>
                                                                    <td>29 - may</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Costo por ciclo:</td>
                                                                    <td>S/ 80.00</td>
                                                                </tr>

                                                            </table>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                                


                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="titulo">
                                            Matrícula
                                        </div>
                                        <div class="desplegable">
                                            <div class="texto">
                                                Una vez que te hayas matriculado, para conectarte a la clase en el horario y frecuencia que elegiste, se te asignará a un grupo de classroom donde encontrarás el enlace para ingresar a la plataforma Zoom.
                                                Se te brindará el material necesario para poder realizar las clases. 
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="titulo">
                                            Certificación
                                        </div>
                                        <div class="desplegable">
                                            <div class="texto">
                                                Al finalizar satisfactoriamente los niveles, se te brindará la certificación a nombre de OMA PERÚ.
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="titulo">
                                            Informes
                                        </div>
                                        <div class="desplegable">
                                            <div class="texto">
                                                Para consultas sobre este programa, envíanos un mensaje a: https://wa.link/jg8i14
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="titulo">
                                            Nuestros docentes
                                        </div>
                                        <div class="desplegable">                                           
                                            <article class="docentes">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <img src="images/quechua-docentes-1.jpg">
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <div class="nombre">
                                                            Katherin Montes Chamorro

                                                        </div>
                                                        <div class="texto">
                                                            Ingeniera de Minas egresada de la Universidad Nacional del Centro Del Perú, becaria del programa Fulbright FLTA. Actualmente estudiante de maestría en la Universidad de Ciencia y Tecnología de Missouri, especialidad de Hidrogeología. Estudiante de la carrera de interpretación y traducción del idioma inglés y francés en Margarita Cabrera Mazur. Experiencia laboral en el sector minero en operaciones de mina, Seguridad y Salud Ocupacional, en el sector energético eléctrico HSE y en el sector construcción e inmobiliaria. Docente del idioma inglés y quechua.

                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <article class="docentes">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <img src="images/quechua-docentes-2.jpg">
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <div class="nombre">
                                                            Yenefer Villanueva 
                                                        </div>
                                                        <div class="texto">
                                                            Ingeniera geóloga, vicepresidenta de Asuntos Culturales de Huancavelica en CIOFF JUVENIL PERÚ. Difusora del idioma en quechua Chanka. Actualmente estudiante becada en la maestría de Geotecnia en PUC-RIO, Brasil.                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <article class="docentes">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <img src="images/quechua-docentes-3.jpg">
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <div class="nombre">
                                                            Ana Cinthia Flores Ramos
                                                        </div>
                                                        <div class="texto">
                                                            Antropóloga por la Universidad Nacional de San Antonio Abad del Cusco, especialista en Gestión Estratégica de las Relaciones Comunitarias por la Pontificia Universidad Católica del Perú – PUCP.  Fortaleció su aprendizaje del idioma quechua Qollao en el IID – UNSAAC.
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </li>
                                </ul>

                             
                                <div class="caja_gris_mas_info">                                    
                                    <div class="txt">SI BUSCAS MÁS INFORMACIÓN PUEDES SOLICITARLA HACIENDO CLICK EN EL SIGUIENTE BOTÓN</div>
                                    <div class="btn_masinfo">
                                        <a href="contactenos.php" class="full"></a>
                                        Más Información
                                    </div>                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <div class="lista_articulos">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <article class="video_box">
                                    <a href="https://www.youtube.com/watch?v=gfOKZEyA2eI" class="full fancybox_video"></a>
                                    <img src="images/curso-video-img.jpg">
                                </article>
                            </div>
                            <div class="col-sm-6">
                                <article class="hazte_socio">
                                    <a href="actividades-asociacion-oma.php" class="full"></a>
                                    <img src="images/curso-hazte-socio.jpg">
                                </article>
                            </div>
                        </div>
                    </div>
                </div>

              
                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>