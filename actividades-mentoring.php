<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-mentoring.jpg); background-position: top;">
                <div class="container">
                    <div class="box">
                        <h1>
                           El mentoring 
                           
                           </h1>
                           
                        <div class="subtitulo">
                            Desarrolla la autonomía de la  <br>
                            persona para que pueda dirigir <br>
                            su propio proceso de aprendizaje
                            
                        </div>
                        
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_mentorias">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">Actividades</a></li>
                            <li>/ <a href="actividades-mentoring.php" class="activo">Mentoring</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3 order-md-1"><div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral sub_menu">
                                    <li>
                                        <a href="que-hacemos-actividades.php" class="full"></a>
                                        Actividades
                                    </li>
                                    <li>
                                        <a href="actividades-programa-mujeres-roca.php" class="full"></a>
                                        Programa Mujeres Roca
                                    </li>
                                    <li>
                                        <a href="actividades-programa-de-coaching.php" class="full"></a>
                                        Programa de Coaching
                                    </li>
                                    <li>
                                        <a href="actividades-asociacion-oma.php" class="full"></a>
                                        Asociación OMA
                                    </li>
                                    <li class="activo">
                                        <a href="actividades-mentoring.php" class="full"></a>
                                        Mentoring
                                    </li>
                                    <li>
                                        <a href="actividades-curso-de-quechua.php" class="full"></a>
                                        Curso de Quechua
                                    </li>
                                    <li>
                                        <a href="actividades-congreso-oma.php" class="full"></a>
                                        Congreso OMA
                                    </li>
                                    <li>
                                        <a href="actividades-ponencias-para-comunidad-oma.php" class="full"></a>
                                        Ponencias para Comunidad Oma
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="full"></a>
                                        VOLUNTARIADO EN PROYECTOS DE DESARROLLO SOSTENIBLE Y RESPONSABILIDAD SOCIAL
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-de-la-comunidad-oma.php" class="full"></a>
                                        VOLUNTARIADO DE LA COMUNIDAD OMA
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2 class="h2_naranja">
                                    Mentoring
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="texto">
                                    Talleres grupales de mentoring que permiten a los estudiantes estar en contacto con empresarios de trayectoria y reconocido prestigio que comparten sus experiencias de vida y los motivan a alcanzar sus metas. 
                                    Sesiones de mentoring  individual pro bono con mentores nacionales e internacionales bajo la metodología de la Escuela de Mentoring de España.

                                </div>
                                <ul class="testimonio_lista_detalle">
                                    <li>                                        
                                        <div class="b">
                                            <div class="texto">
                                                Estoy terminando mi Certificación en Mentoring con María Luisa, y estoy acompañado varios procesos de Mentoring con tus chicos de OMA. Quiero agradecerte y reconocerte el maravilloso proyecto que pusiste en marcha!! 
                                            </div>
                                            <div class="nombre">
                                                Nuria Povill
                                            </div>
                                            <div class="cargo">
                                                Mentora  de la  Escuela de Mentoring de España <br>
                                                Socia Directora SOLO Consultores.  <br>
                                                Totales- GR Institut Israel Barcelona, Cartaluña, España
                                            </div>
                                        </div>
                                        <div class="foto">
                                            <img src="images/foto-nuria-povill-testimonio.png">
                                        </div>
                                    </li>
                                </ul>
                                <div class="lista_logros_fotografias">
                                    <div class="row">
                                        <div class="col-12 col-md-4">
                                            <img src="images/mentoring-1.jpg">

                                            <img src="images/mentoring-4.jpg">
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/mentoring-2.jpg">

                                            <img src="images/mentoring-5.jpg">
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/mentoring-3.jpg">

                                            <img src="images/mentoring-6.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php include 'seccion_testimonios.html';?>

                <?php include 'seccion-nuestro-blog.html';?>






            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>