<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_fondo_red">				
                <div class="container">
                    <div class="box">
                        <div class="tablet">
                            <img src="images/ponencias-tablet.png">
                        </div>
                        <h1>
                            PONENCIAS PARA 
                            LA COMUNIDAD 
                            OMA DIGITAL
                        </h1>
                        <div class="subtitulo">
                            Habilidades blandas, Experiencias de vida, Tecnología, Responsabilidad social, Emprendedurismo, Innovación.
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">Actividades</a></li>
                            <li>/ <a href="actividades-ponencias-para-comunidad-oma.php" class="activo"> Ponencias para Comunidad Oma</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3  order-md-1">
                                <div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral sub_menu">
                                    <li>
                                        <a href="que-hacemos-actividades.php" class="full"></a>
                                        Actividades
                                    </li>
                                    <li>
                                        <a href="actividades-programa-mujeres-roca.php" class="full"></a>
                                        Programa Mujeres Roca
                                    </li>
                                    <li>
                                        <a href="actividades-programa-de-coaching.php" class="full"></a>
                                        Programa de Coaching
                                    </li>
                                    <li>
                                        <a href="actividades-asociacion-oma.php" class="full"></a>
                                        Asociación OMA
                                    </li>
                                    <li>
                                        <a href="actividades-mentoring.php" class="full"></a>
                                        Mentoring
                                    </li>
                                    <li>
                                        <a href="actividades-curso-de-quechua.php" class="full"></a>
                                        Curso de Quechua
                                    </li>
                                    <li>
                                        <a href="actividades-congreso-oma.php" class="full"></a>
                                        Congreso OMA
                                    </li>
                                    <li class="activo">
                                        <a href="actividades-ponencias-para-comunidad-oma.php" class="full"></a>
                                        Ponencias para Comunidad Oma
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="full"></a>
                                        VOLUNTARIADO EN PROYECTOS DE DESARROLLO SOSTENIBLE Y RESPONSABILIDAD SOCIAL
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-de-la-comunidad-oma.php" class="full"></a>
                                        VOLUNTARIADO DE LA COMUNIDAD OMA
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2 class="h2_naranja">
                                     Ponencias para Comunidad Oma
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="texto">
                                    <p>Ponencias semanales a cargo de destacados profesionales del sector enfocadas en habilidades blandas, innovación, fortalecimiento de valores y temas relevantes al sector minero-energético con la finalidad de mantenerlos informados y motivarlos a seguir aprendiendo y capacitándose.</p>

                                    <p>Las ponencias se desarrollan todas las semanas los días miércoles a las 3:00 p.m. y se transmiten en vivo por Facebook. </p>


                                </div>
                                <div class="seccion_charlas_de_hoy">
                                    <article>
                                        <div class="imagen">
                                            <img src="images/ponencias-1.png">
                                        </div>
                                        <div class="c">
                                            <div class="tit">
                                                EXPERIENCIAS - TRANSICIÓN DE UNA EMPRESA MINERA A OTRA
                                            </div>
                                            <div class="fecha">
                                                Miércoles 09 de febrero las 3:00 p.m.
                                            </div>
                                            <div class="texto">
                                                Dirigido por la ingeniera <strong>Nieves Ayvar</strong> quien es Management - Sector Minero Extractivo | Mentora Organizacional Certificada | Mujer Líder en el Sector Minero Peruano - Banco Interamericano de Desarrollo | Candidata a MBA -Centrum. PUCP
                                            </div>

                                            <div class="btn_vervideo">
                                                <a href="https://fb.watch/cDTQ1T9fr9/" target="_blank" class="full"></a>
                                                Ver video
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="imagen">
                                            <img src="images/ponencias-2.png">
                                        </div>
                                        <div class="c">
                                            <div class="tit">
                                                Minería para alcanzar el desarrollo
                                            </div>
                                            <div class="fecha">
                                                Miércoles 26 de enero a las 3:00 p.m.
                                            </div>
                                            <div class="texto">
                                                Dirigido por <strong>Guillermo Vidalón</strong> quien es Superintendente de Relaciones

                                            </div>
                                            <div class="btn_vervideo">
                                                <a href="https://fb.watch/cDTU1uqwoF/" target="_blank" class="full"></a>
                                                Ver video
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="imagen">
                                            <img src="images/ponencias-3.png">
                                        </div>
                                        <div class="c">
                                            <div class="tit">
                                                ACTITUDES POSITIVAS VERSUS LA ADVERSIDAD
                                            </div>
                                            <div class="fecha">
                                                Miércoles 01 de setiembre a las 3:00 p.m.
                                            </div>
                                            <div class="texto">
                                                Dirigido por <strong>Lic. Isabel Arias Vargas</strong> quien es presidenta del Directorio de Compañía Minera San Ignacio de Morococha S.A.A.
                                            </div>
                                            <div class="btn_vervideo">
                                                <a href="https://fb.watch/cDTVLmr8YF/" target="_blank" class="full"></a>
                                                Ver video
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="imagen">
                                            <img src="images/ponencias-4.png">
                                        </div>
                                        <div class="c">
                                            <div class="tit">
                                                EXPERIENCIAS QUE INSPIRAN
                                            </div>
                                            <div class="fecha">
                                                Miércoles 25 de noviembre 2020 a las 3:00 p.m.

                                            </div>
                                            <div class="texto">
                                                Dirigido por el <strong>MBA Adolfo F. Vera,</strong> quien es President & CEO AT Southern Peaks Minign Ltd, en donde opera la a Compañía Minera Condestable, y Ariana Operaciones Mineras.
                                            </div>
                                            <div class="btn_vervideo">
                                                <a href="https://fb.watch/cDTXiAxcoA/" target="_blank" class="full"></a>
                                                Ver video
                                            </div>
                                        </div>
                                    </article>
                                    <article>
                                        <div class="imagen">
                                            <img src="images/ponencias-5.png">
                                        </div>
                                        <div class="c">
                                            <div class="tit">
                                                EXPERIENCIAS QUE INSPIRAN
                                            </div>
                                            <div class="fecha">
                                                El 21 de octubre del 2020
                                            </div>
                                            <div class="texto">
                                                Invitado al <strong>Sr. José Elejalde</strong> gerente de finanzas administración y comercialización de minera poderosa

                                            </div>
                                            <div class="btn_vervideo">
                                                <a href="https://fb.watch/cDT-hAtroP/" target="_blank" class="full"></a>
                                                Ver video
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                
                            </div>                            
                        </div>
                    </div>
                </div>

               

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>