<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-programa-de-coaching.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            APRENDIZAJE QUE <br>
                            TRANSFORMA VIDAS
                        </h1>
                        <div class="subtitulo">
                            El talento está formado por la suma de conocimientos, <br>
                            competencias blandas, ética, compromiso y acción.
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_coaching">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">Actividades</a></li>
                            <li>/ <a href="actividades-programa-de-coaching.php" class="activo">Programa de Coaching</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">                            
                            <div class="col-md-3 order-md-1"><div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral sub_menu">
                                    <li>
                                        <a href="que-hacemos-actividades.php" class="full"></a>
                                        Actividades
                                    </li>
                                    <li>
                                        <a href="actividades-programa-mujeres-roca.php" class="full"></a>
                                        Programa Mujeres Roca
                                    </li>
                                    <li class="activo">
                                        <a href="actividades-programa-de-coaching.php" class="full"></a>
                                        Programa de Coaching
                                    </li>
                                    <li>
                                        <a href="actividades-asociacion-oma.php" class="full"></a>
                                        Asociación OMA
                                    </li>
                                    <li>
                                        <a href="actividades-mentoring.php" class="full"></a>
                                        Mentoring
                                    </li>
                                    <li>
                                        <a href="actividades-curso-de-quechua.php" class="full"></a>
                                        Curso de Quechua
                                    </li>
                                    <li>
                                        <a href="actividades-congreso-oma.php" class="full"></a>
                                        Congreso OMA
                                    </li>
                                    <li>
                                        <a href="actividades-ponencias-para-comunidad-oma.php" class="full"></a>
                                        Ponencias para Comunidad Oma
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="full"></a>
                                        VOLUNTARIADO EN PROYECTOS DE DESARROLLO SOSTENIBLE Y RESPONSABILIDAD SOCIAL
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-de-la-comunidad-oma.php" class="full"></a>
                                        VOLUNTARIADO DE LA COMUNIDAD OMA
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2 class="h2_naranja">
                                    Programa de Coaching
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="texto">
                                    


                                    <p>Sesiones grupales de aprendizaje teórico-práctico sobre objetivos puntuales de competencias blandas y metodologías que aportan al desarrollo humano enfocado en el campo profesional, empleabilidad, responsabilidad social, entre otros temas relevantes.
                                    Cada taller de coaching es diseñado de acuerdo con las necesidades específicas de la entidad que lo solicita. La duración de cada taller es de 4 horas de forma presencial y 2 horas con 30 minutos de manera virtual. Los temas más solicitados son:
                                        </p>

                                    <p>
                                         <span class="orange capi">Desarrollo de Competencias para la Inserción Laboral y Empleabilidad </span> <br>
                                    <strong>Objetivos:</strong> <br>
                                    Preparar a los estudiantes para que asuman con responsabilidad los distintos retos que se presenten en su actividad profesional, mediante el desarrollo de sus habilidades de liderazgo, trabajo en equipo, gestión de conflictos e inteligencia emocional.
                                    Proporcionar a los estudiantes las herramientas y técnicas necesarias para que puedan diferenciarse y destacar en los procesos de selección de personal. 
                                    </p>

                                    <p>
                                         <span class="orange capi">Desarrollo de Competencias para el Docente Universitario </span> <br>
                                    <strong>Objetivos:</strong> <br>
                                    Desarrollar estrategias para que los docentes puedan motivar a sus alumnos y brindarles técnicas de aprendizaje participativo que puedan ser adaptadas y replicadas en las aulas del campus universitario como virtual.
                                    </p>

                                    <p>
                                         <span class="orange capi">Inducción a la Vida Universitaria </span> <br>
                                    <strong>Objetivos:</strong> <br>
                                    Brindar a los estudiantes que ingresan a la universidad las herramientas para enfrentar con éxito los desafíos de la vida universitaria.
                                    Fomentar el desarrollo de competencias para el liderazgo y potenciar sus habilidades para el trabajo en equipo.
                                    </p>
                                    
                                    <p>
                                         <span class="orange capi">Liderazgo en Valores para la Comunidad </span> <br>
                                    <strong>Objetivos:</strong> <br>
                                    Mejorar la capacidad de comunicación con las empresas que trabajan en la zona de influencia y fomentar el desarrollo de competencias que faciliten el trabajo en equipo. Sensibilizar a los integrantes de las comunidades campesinas sobre la importancia de fortalecer la ética en el entorno en el que se desenvuelven asumiendo liderazgo y reforzando su autoestima.
                                    </p>
                                    
                                    <p>Difundir los beneficios de la minería responsable

                                    </p>
                                    
                                    <p>Desarrollar las habilidades de liderazgo y comunicación efectiva en los estudiantes y docentes

                                    </p>

                                    <p>
                                         <span class="orange capi">Liderazgo Social y Comunicación Productiva en la Era Digital </span> <br>
                                    <strong>Objetivos:</strong> <br>
                                    Desarrollar en las participantes competencias que impulsen y faciliten sus labores de voluntariado y responsabilidad social, así como una adecuada comunicación con las comunidades, superando las dificultades de la virtualidad y usando estrategias digitales.
                                    </p>

                                    <p>
                                         <span class="orange capi">Desarrollo de competencias para el liderazgo, comunicación efectiva y trabajo en equipo </span> <br>
                                    <strong>Objetivos:</strong> <br>
                                    Incentivar a los estudiantes a tener un mayor conocimiento de sí mismos. Brindar herramientas que les permitan generar una comunicación eficaz y transmitir sus ideas de manera clara y coherente
                                    Enseñar a los estudiantes las estrategias efectivas para liderar y motivar adecuadamente a un equipo de trabajo
                                    </p>
                                    
                                    <p>
                                        <span class="orange capi">Otros talleres cortos sobre competencias específicas:</span>

                                    </p>

                                    <ul class="lista_check_orange">
                                        <li>Metodologías para el Aprendizaje Presencial y Virtual</li>
                                        <li>Marca Personal para la Empleabilidad</li>
                                        <li>Desarrollo de la Identidad Digital</li>
                                        <li>Planeamiento Estratégico en la Era Digital</li>
                                        <li>Elaboración de un CV de Impacto</li>
                                        <li>Estrategias para Realizar el Video de Presentación Profesional</li>
                                        <li>Storytelling para la Empleabilidad</li>
                                    </ul>
 



                                </div>

                                

                                <div class="galeria">
                                    <div class="row">
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/coaching-1.jpg"></div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/coaching-2.jpg"></div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/coaching-3.jpg"></div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/coaching-4.jpg"></div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/coaching-5.jpg"></div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/coaching-6.jpg"></div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/coaching-7.jpg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php include 'seccion_testimonios.html';?>

                <?php include 'seccion-nuestro-blog.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>