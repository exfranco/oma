<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_especial"  style="background-image: url(images/banner-mujeres-roca.jpg);">				
                <div class="container">
                    <div class="box">
                        <div class="logo_mujeres_roca">
                            <img src="images/logo-mujeres-roca.png">
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                    <div class="logo_duo">
                        <img src="images/logo-duo.png">
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">Actividades</a></li>
                            <li>/ <a href="actividades-programa-mujeres-roca.php" class="activo">Programa mujeres roca</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3 order-md-1"><div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral sub_menu">
                                    <li>
                                        <a href="que-hacemos-actividades.php" class="full"></a>
                                        Actividades
                                    </li>
                                    <li class="activo">
                                        <a href="actividades-programa-mujeres-roca.php" class="full"></a>
                                        Programa Mujeres Roca
                                    </li>
                                    <li>
                                        <a href="actividades-programa-de-coaching.php" class="full"></a>
                                        Programa de Coaching
                                    </li>
                                    <li>
                                        <a href="actividades-asociacion-oma.php" class="full"></a>
                                        Asociación OMA
                                    </li>
                                    <li>
                                        <a href="actividades-mentoring.php" class="full"></a>
                                        Mentoring
                                    </li>
                                    <li>
                                        <a href="actividades-curso-de-quechua.php" class="full"></a>
                                        Curso de Quechua
                                    </li>
                                    <li>
                                        <a href="actividades-congreso-oma.php" class="full"></a>
                                        Congreso OMA
                                    </li>
                                    <li>
                                        <a href="actividades-ponencias-para-comunidad-oma.php" class="full"></a>
                                        Ponencias para Comunidad Oma
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="full"></a>
                                        VOLUNTARIADO EN PROYECTOS DE DESARROLLO SOSTENIBLE Y RESPONSABILIDAD SOCIAL
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-de-la-comunidad-oma.php" class="full"></a>
                                        VOLUNTARIADO DE LA COMUNIDAD OMA
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2 class="h2_naranja">
                                    Programa mujeres roca
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="texto">
                                    <p>Impulsado por OMA en alianza con la Cámara de Comercio Canadá Perú, el programa “Mujeres Roca” busca promover la participación de la mujer en puestos ejecutivos en el sector minero-energético del Perú. </p>

                                    <p>
                                        La meta es crear una comunidad selecta de líderes referentes que promocione el liderazgo de las mujeres en el sector, brindarles orientación profesional y fortalecer su talento y habilidades. Asimismo, busca reforzar el interés y compromiso de los ejecutivos mineros en consolidar políticas de equidad de género y estrategias de captación de talento femenino.
                                        Además de fortalecer, apoyar y potenciar habilidades, visibiliza a las jóvenes con alto potencial en las empresas nacionales y extranjeras establecidas en el país. Busca, también, conectarlas y mantenerlas informadas sobre capacitaciones, vacantes y diversas actividades formadoras a través de una red de intercambio profesional. 
                                        El programa crea oportunidades de capacitación y mentoring para 10 mujeres cuidadosamente
                                        seleccionadas a nivel nacional. Propicia un espacio de diálogo con profesionales destacados del sector en el que además de compartir experiencias se identifican las cualidades que les permitirán alcanzar sus metas. 
                                    </p>
                                    
                                    <p>Combina el coaching y el mentoring grupal e individual. Comprende un taller de empoderamiento, liderazgo y desarrollo de carrera, llamado “Kallpa Warmi”, un taller de empleabilidad, una visita técnica, sesiones de mentoring grupal con destacados ejecutivos del sector y sesiones individuales de mentoring durante seis meses a modo de seguimiento y soporte. </p>

                                </div>
                                <div class="lista_logros_fotografias">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="video">
                                                <div class="contenedor_video">
                                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/kxtlhRqGEY4" title="Mujeres Roca 2016" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/mujeres-roca-1.jpg">                                            
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/mujeres-roca-2.jpg">                                            
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/mujeres-roca-3.jpg">                                            
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/mujeres-roca-4.jpg">
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/mujeres-roca-5.jpg">
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/mujeres-roca-6.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>

                <?php include 'seccion_testimonios.html';?>

                <?php include 'seccion-nuestro-blog.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>