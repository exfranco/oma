<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_especial" style="background-image: url(images/banner-voluntariado-comunidad-oma.jpg);">				
                <div class="container">
                    <div class="box">                        
                        <h1>
                            Bienvenidos al voluntariado <br>
                            de la Comunidad OMA 
                        </h1>
                        
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">Actividades</a></li>
                            <li>/ <a href="actividades-voluntariado-de-la-comunidad-oma.php" class="activo">Voluntariado de la comunidad OMA</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3 order-md-1">
                                <div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral sub_menu">
                                    <li>
                                        <a href="que-hacemos-actividades.php" class="full"></a>
                                        Actividades
                                    </li>
                                    <li>
                                        <a href="actividades-programa-mujeres-roca.php" class="full"></a>
                                        Programa Mujeres Roca
                                    </li>
                                    <li>
                                        <a href="actividades-programa-de-coaching.php" class="full"></a>
                                        Programa de Coaching
                                    </li>
                                    <li>
                                        <a href="actividades-asociacion-oma.php" class="full"></a>
                                        Asociación OMA
                                    </li>
                                    <li>
                                        <a href="actividades-mentoring.php" class="full"></a>
                                        Mentoring
                                    </li>
                                    <li>
                                        <a href="actividades-curso-de-quechua.php" class="full"></a>
                                        Curso de Quechua
                                    </li>
                                    <li>
                                        <a href="actividades-congreso-oma.php" class="full"></a>
                                        Congreso OMA
                                    </li>
                                    <li>
                                        <a href="actividades-ponencias-para-comunidad-oma.php" class="full"></a>
                                        Ponencias para Comunidad Oma
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="full"></a>
                                        VOLUNTARIADO EN PROYECTOS DE DESARROLLO SOSTENIBLE Y RESPONSABILIDAD SOCIAL
                                    </li>
                                    <li class="activo">
                                        <a href="actividades-voluntariado-de-la-comunidad-oma.php" class="full"></a>
                                        VOLUNTARIADO DE LA COMUNIDAD OMA
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2 class="h2_naranja">
                                    Voluntariado de la comunidad OMA
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="texto">
                                    <p>Convocamos a estudiantes que nos apoyan voluntariamente en la logística y coordinación de los eventos OMA, y les otorgamos certificados como colaboradores voluntarios de OMA. Programa que recluta y capacita a jóvenes emprendedores voluntarios y pone a disposición de los empresarios y reclutadores sus perfiles e información de contacto.</p>
                                </div>
                                <div class="lista_logros_fotografias">
                                    <div class="row">
                                        <div class="col-12 col-md-4">
                                            <img src="images/voluntariado-comunidad-oma-1.jpg">
                                            <img src="images/voluntariado-comunidad-oma-4.jpg">
                                            <img src="images/voluntariado-comunidad-oma-7.jpg">
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/voluntariado-comunidad-oma-2.jpg">
                                            <img src="images/voluntariado-comunidad-oma-5.jpg">
                                            <img src="images/voluntariado-comunidad-oma-8.jpg">
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/voluntariado-comunidad-oma-3.jpg">
                                            <img src="images/voluntariado-comunidad-oma-6.jpg">
                                            <img src="images/voluntariado-comunidad-oma-9.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>

                

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>

                <?php include 'seccion_testimonios.html';?>

                <?php include 'seccion-nuestro-blog.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>