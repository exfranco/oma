<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner" style="background-image: url(images/banner-proyectos-de-responsabilidad.jpg);">				
                <div class="container">
                    <div class="box">                        
                        <h1>
                            PROYECTOS DE <br>
                            RESPONSABILIDAD <br>
                            SOCIAL QUE AYUDAN AL <br>
                            CRECIMIENTO <br>
                            DE NUESTRO PAÍS 
                        </h1>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">Actividades</a></li>
                            <li>/ <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="activo">  Voluntariado en proyectos de desarrollo sostenible y Responsabilidad social</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3  order-md-1">
                                <div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral sub_menu">
                                    <li>
                                        <a href="que-hacemos-actividades.php" class="full"></a>
                                        Actividades
                                    </li>
                                    <li>
                                        <a href="actividades-programa-mujeres-roca.php" class="full"></a>
                                        Programa Mujeres Roca
                                    </li>
                                    <li>
                                        <a href="actividades-programa-de-coaching.php" class="full"></a>
                                        Programa de Coaching
                                    </li>
                                    <li>
                                        <a href="actividades-asociacion-oma.php" class="full"></a>
                                        Asociación OMA
                                    </li>
                                    <li>
                                        <a href="actividades-mentoring.php" class="full"></a>
                                        Mentoring
                                    </li>
                                    <li>
                                        <a href="actividades-curso-de-quechua.php" class="full"></a>
                                        Curso de Quechua
                                    </li>
                                    <li>
                                        <a href="actividades-congreso-oma.php" class="full"></a>
                                        Congreso OMA
                                    </li>
                                    <li>
                                        <a href="actividades-ponencias-para-comunidad-oma.php" class="full"></a>
                                        Ponencias para Comunidad Oma
                                    </li>
                                    <li class="activo">
                                        <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="full"></a>
                                        VOLUNTARIADO EN PROYECTOS DE DESARROLLO SOSTENIBLE Y RESPONSABILIDAD SOCIAL
                                    </li>
                                    <li>
                                        <a href="actividades-voluntariado-de-la-comunidad-oma.php" class="full"></a>
                                        VOLUNTARIADO DE LA COMUNIDAD OMA
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2 class="h2_naranja">
                                      Voluntariado en proyectos de desarrollo sostenible y Responsabilidad social
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="texto">
                                    <p>Proyectos que se desarrollan en las regiones del país cargo de los voluntarios de la Comunidad OMA.</p>

                                    <p><span class="mayor">1. Proyecto Servir: </span> <br>
                                    Capacitar, apoyar y coordinar con los voluntarios y las Ugeles el acompañamiento a los escolares de primaria y secundaria de las zonas más necesitadas del país, a fin de contribuir a su nivelación y regreso a clases presenciales.</p>

                                    <p><span class="mayor">2. Mejoramiento del rendimiento escolar y desarrollo de habilidades para la vida.</span></p>

                                    <p><span class="mayor">3. Innovación sostenible en el cultivo de los principales productos de alimentación saludable nativa.</span></p>

                                    <p><span class="mayor">4. Investigación en proyectos de responsabilidad social en empresas mineras</span></p>
                                </div>
                                <div class="lista_logros_fotografias">
                                    <div class="row">
                                        <div class="col-12 col-md-4">
                                            <img src="images/voluntariado-img-1.jpg">

                                            <img src="images/voluntariado-img-4.jpg">
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/voluntariado-img-2.jpg">
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <img src="images/voluntariado-img-3.jpg">                                            
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>

                

                <?php include 'seccion-nuestro-blog.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>