<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
     <?php include 'metas.html';?>

</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_chico"  style="background-image: url(images/banner-becas.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            <span>ASOCIACIÓN OMA</span> <br>
                            BECAS
                        </h1>
                        <div class="subtitulo">
                            El talento está formado por la suma de conocimientos, <br>
                            competencias blandas, ética, compromiso y acción.
                        </div>
                    </div>
                </div>								
			</div>

            <section class="seccion_becas">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="becas.php" class="activo">Becas</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="lista_becas">
                    <div class="container">
                        <div class="row">   
                            <div class="col-md-9">
                                <h3>
                                    subTitulo de la Beca
                                </h3>
                                <h2>
                                    Titulo de la Beca
                                </h2>

                                
                                <article class="blog_detalle">                                    
                                    <div class="imagen">
                                        <img src="images/blog-1.jpg">
                                    </div>
                                    <div class="b">
                                        <div class="tit">
                                            Nuevas Formas Digitales de Comunicación
                                        </div>
                                        
                                        <div class="pie">
                                            <div class="autor">
                                                Eddy Monje
                                            </div>
                                            <div class="fecha">
                                                05/01/2022
                                            </div>
                                        </div>
                                        <div class="texto">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec consectetur velit et sapien lobortis, vitae maximus dolor faucibus. Suspendisse potenti. Donec commodo sollicitudin dui ac faucibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. In hac habitasse platea dictumst. Vivamus bibendum, neque eu luctus hendrerit, lacus massa vulputate lacus, sit amet pretium tortor lacus non enim. Maecenas enim metus, posuere sit amet dui at, malesuada elementum neque. Quisque lobortis nisl eget leo elementum condimentum. Donec tincidunt at massa id sollicitudin. Integer non congue libero, ac pulvinar ligula.</p>

                                            <p>Donec condimentum libero eu nibh tincidunt, nec facilisis nisi euismod. Suspendisse potenti. Praesent massa neque, tincidunt vel porttitor sit amet, mollis a lorem. In nec nibh ex. Duis in convallis tortor, sed ullamcorper eros. Sed urna elit, varius eu nisl id, fermentum suscipit felis. Nunc a metus odio. Donec aliquet tincidunt ex id euismod. Phasellus in sagittis enim. Sed et elit in quam pharetra efficitur vitae non odio. Donec sollicitudin et risus vel mollis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>

                                            <p>Curabitur nec urna eu nunc dapibus tincidunt. Phasellus lectus ante, consequat id malesuada vitae, vulputate eu lorem. Aenean at ex non nisi ultricies aliquam. Maecenas enim tortor, viverra a accumsan nec, mattis nec justo. Sed non dui ac nunc rhoncus egestas sed eu nisl. Suspendisse interdum neque a aliquet venenatis. Vivamus eget diam risus. Fusce eu tellus risus. Proin mattis nunc neque, et gravida ex sollicitudin ac. Mauris fermentum commodo laoreet. Morbi eget elit at quam fermentum lacinia. Curabitur libero mauris, tincidunt eget erat sit amet, dictum volutpat urna. Mauris ultrices sit amet diam ut fermentum. Sed erat quam, mattis non urna vel, tincidunt mollis leo. Mauris et porta erat.</p>

                                            <p>Phasellus malesuada orci sagittis mauris viverra, vel ultrices elit placerat. Phasellus et velit eu elit tincidunt iaculis nec vel ex. Sed eget consectetur magna. Integer faucibus erat cursus dui pulvinar, sit amet vulputate dolor accumsan. Sed non sem mauris. Nunc quis luctus sem, quis congue purus. Donec laoreet rhoncus malesuada. Praesent laoreet elementum ornare. Donec risus felis, ultrices sed ex sed, bibendum rhoncus purus. Sed dictum ultrices eros quis mollis. Integer auctor ligula eu neque blandit eleifend. In quis ipsum feugiat magna auctor dignissim. Aliquam sit amet vehicula turpis, quis laoreet est. Integer id purus gravida, fermentum justo at, sodales enim. Curabitur porta semper diam.</p>

                                            <p>
                                                <div class="imagen_flotante_left">
                                                    <img src="images/blog-detalle-img-1.jpg">
                                                </div>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec consectetur velit et sapien lobortis, vitae maximus dolor faucibus. Suspendisse potenti. Donec commodo sollicitudin dui ac faucibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. In hac habitasse platea dictumst. Vivamus bibendum, neque eu luctus hendrerit, lacus massa vulputate lacus, sit amet pretium tortor lacus non enim. Maecenas enim metus, posuere sit amet dui at, malesuada elementum neque. Quisque lobortis nisl eget leo elementum condimentum. Donec tincidunt at massa id sollicitudin. Integer non congue libero, ac pulvinar ligula.

                                            </p>

                                            
                                            <p>Donec condimentum libero eu nibh tincidunt, nec facilisis nisi euismod. Suspendisse potenti. Praesent massa neque, tincidunt vel porttitor sit amet, mollis a lorem. In nec nibh ex. Duis in convallis tortor, sed ullamcorper eros. Sed urna elit, varius eu nisl id, fermentum suscipit felis. Nunc a metus odio. Donec aliquet tincidunt ex id euismod. Phasellus in sagittis enim. Sed et elit in quam pharetra efficitur vitae non odio. Donec sollicitudin et risus vel mollis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>

                                            <p>Curabitur nec urna eu nunc dapibus tincidunt. Phasellus lectus ante, consequat id malesuada vitae, vulputate eu lorem. Aenean at ex non nisi ultricies aliquam. Maecenas enim tortor, viverra a accumsan nec, mattis nec justo. Sed non dui ac nunc rhoncus egestas sed eu nisl. Suspendisse interdum neque a aliquet venenatis. Vivamus eget diam risus. Fusce eu tellus risus. Proin mattis nunc neque, et gravida ex sollicitudin ac. Mauris fermentum commodo laoreet. Morbi eget elit at quam fermentum lacinia. Curabitur libero mauris, tincidunt eget erat sit amet, dictum volutpat urna. Mauris ultrices sit amet diam ut fermentum. Sed erat quam, mattis non urna vel, tincidunt mollis leo. Mauris et porta erat.</p>

                                            <p>Phasellus malesuada orci sagittis mauris viverra, vel ultrices elit placerat. Phasellus et velit eu elit tincidunt iaculis nec vel ex. Sed eget consectetur magna. Integer faucibus erat cursus dui pulvinar, sit amet vulputate dolor accumsan. Sed non sem mauris. Nunc quis luctus sem, quis congue purus. Donec laoreet rhoncus malesuada. Praesent laoreet elementum ornare. Donec risus felis, ultrices sed ex sed, bibendum rhoncus purus. Sed dictum ultrices eros quis mollis. Integer auctor ligula eu neque blandit eleifend. In quis ipsum feugiat magna auctor dignissim. Aliquam sit amet vehicula turpis, quis laoreet est. Integer id purus gravida, fermentum justo at, sodales enim. Curabitur porta semper diam.</p>

                                        </div>
                                        
                                    </div>
                                </article>

                                <div class="btn_regresar">
                                    <a href="becas.php" class="full"></a>
                                    Regresar
                                </div>
                                
                            </div>
                            <div class="col-md-3">
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>
 
</body>
</html>