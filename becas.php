<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-becas.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>                            
                            BECAS
                        </h1>
                        <div class="subtitulo">
                            El talento está formado por la suma de conocimientos, <br>
                            competencias blandas, ética, compromiso y acción.
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_becas">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="becas.php" class="activo">Becas</a></li>
                        </ul>
                    </div>                   											
                </div>

                
                <div class="container">
                    <div class="row">
                        
                        <div class="col-12">
                            <h2>
                                Becas
                            </h2>
                            <div class="lista_standar">
                                <div class="row">
                                    <div class="col-sm-6 col-md-3">
                                        <article>                                            
                                            <div class="imagen">
                                                <img src="images/becas-1.jpg">
                                            </div>
                                            <div class="tit">
                                                CETEMIN
                                            </div>
                                           <div class="texto">
                                                20 medias becas de CETEMIN, único Instituto licenciado por el Minedu con carreras técnicas de 11 meses.
                                            </div>

                                            <div class="btn_info">
                                                <a href="#" onclick="layerLogin()" class="full"></a>
                                                Hazte Socio
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <article>                                            
                                            <div class="imagen">
                                                <img src="images/becas-2.jpg">
                                            </div>
                                            <div class="tit">
                                                ISE ACADEMY
                                            </div>
                                           <div class="texto">
                                                5  medias becas de inglés del Instituto Peruano Norteamericano
                                            </div>

                                            <div class="btn_info">
                                                <a href="#" onclick="layerLogin()" class="full"></a>
                                                Hazte Socio
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <article>                                            
                                            <div class="imagen">
                                                <img src="images/becas-3.jpg">
                                            </div>
                                            <div class="tit">
                                                CAMARA DE COMERCIO ALEMANA
                                            </div>
                                           <div class="texto">
                                                5 becas en cursos de tecnología e innovación de la Cámara de Comercio Alemana Peruana 
                                            </div>

                                            <div class="btn_info">
                                                <a href="#" onclick="layerLogin()" class="full"></a>
                                                Hazte Socio
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <article>                                            
                                            <div class="imagen">
                                                <img src="images/becas-4.jpg">
                                            </div>
                                            <div class="tit">
                                                DATA SCIENCE
                                            </div>
                                           <div class="texto">
                                                15 becas de Data Science: Introducción a la ciencia de datos
                                            </div>

                                            <div class="btn_info">
                                                <a href="#" onclick="layerLogin()" class="full"></a>
                                                Hazte Socio
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <article>                                            
                                            <div class="imagen">
                                                <img src="images/becas-5.jpg">
                                            </div>
                                            <div class="tit">
                                                ESCUELA DE MENTORING DE ESPAÑA
                                            </div>
                                           <div class="texto">
                                                12 becas de la Escuela de Mentoring de España
                                            </div>

                                            <div class="btn_info">
                                                <a href="#" onclick="layerLogin()" class="full"></a>
                                                Hazte Socio
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <article>                                            
                                            <div class="imagen">
                                                <img src="images/becas-6.jpg">
                                            </div>
                                            <div class="tit">
                                                UNIVERSIDAD DE CELAYA
                                            </div>
                                           <div class="texto">
                                                Maestrias online, pasantías de la Universidad de Celaya. (México y EEUU)
                                            </div>

                                            <div class="btn_info">
                                                <a href="#" onclick="layerLogin()" class="full"></a>
                                                Hazte Socio
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>
    <div class="layer_login" id="layerLogin">
        <div class="sombra_layer"></div>
        <div class="caja">
            <div class="close"></div>
            <div class="c">
                <div class="titulo">
                    Registrate
                </div>
                <div class="parrafo">
                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque 
                </div>
                <div class="formulario">
                    <div class="campo">
                        <input type="text" placeholder="USUARIO">
                    </div>
                    <div class="campo">
                        <input type="password" placeholder="CLAVE">
                    </div>
                    <div class="btn_ingresar">
                        <a href="becas-detalle.php" class="full"></a>
                        Ingresar
                    </div>
                    <div class="pie">
                        <div class="left">
                            <div class="btn_texto">
                                Olvidé mi clave
                            </div>
                            <div class="btn_texto">
                                No tengo usuario y clave
                            </div>
                        </div>
                        <div class="btn_close" onclick="closeLayerLogin()">
                            Cerrar
                        </div>
                    </div>
                </div>
            </div>                                                            
        </div>                                                        
    </div>

</body>
</html>