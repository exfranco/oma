<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
     <?php include 'metas.html';?>

</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_chico" style="background-image: url(images/banner-blog.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            Blog
                        </h1>
                        <div class="subtitulo">
                            OMA reconoce que la innovación es un eje central <br>
                            para el fortalecimiento económico y social.
                        </div>
                    </div>
                </div>								
			</div>

            <section class="seccion_blog">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="blog.php" class="activo">Blog</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="lista_blog">
                    <div class="container">
                        <div class="row">   
                            <div class="col-md-9">
                                <h3>
                                    Mantente Actualizado
                                </h3>
                                <h2>
                                    Nuestro Blog y Noticias
                                </h2>
                                
                                <article class="blog_detalle">                                    
                                    <div class="imagen">
                                        <img src="images/blog-1.jpg">
                                    </div>
                                    <div class="b">
                                        <div class="tit">
                                            Creencias limitantes: la lucha de la pulga contra el elefante
                                        </div>
                                        
                                        <div class="pie">
                                            <div class="autor">
                                                
                                            </div>
                                            <div class="fecha">
                                                
                                            </div>
                                        </div>
                                        <div class="texto">
                                            <p>¿Te imaginas una lucha entre una pulga y un elefante? Sí, aparte de ser una batalla perdida que no te llevará a ganar ninguna guerra, es una lucha agotadora y desgastante. La misma lucha que todos los días peleamos, sin ser conscientes de ello, entre lo que realmente queremos conseguir y lo que nuestras creencias limitantes nos dejan.</p>
                                            <p>Supongo que a estas alturas todos conocemos la metáfora del iceberg y la mente. Habremos escuchado muchas veces que nuestra mente es un como un iceberg, es decir, nuestra mente consciente, es una parte muy ínfima de la totalidad. Que abajo de la superficie hay un mundo inabarcable de emociones reprimidas, impulsores de la personalidad, miedos, creencia limitantes, trampas mentales, prejuicios, y otras malezas como diría Sócrates. Que esa parte subterránea es nuestra mente inconsciente, la que coge el timón de nuestra vida sin avisar en muchas ocasiones. La responsable de esas decisiones, esas acciones y resultados, que a veces no entendemos.</p>
                                            <p>Pensemos en esa zona subterránea del iceberg como si fuera un elefante, y en la punta del iceberg como si fuera una pulga montada en un elefante. ¿Te imaginas quién va a decidir realmente hacia dónde ir?  Por muy fastidiosa e insistente que se ponga la pulga con órdenes, indicaciones, argumentos, contrargumentos, poniendo toda su pasión, energía, pensamiento positivo, y lo que pueda caber en el cuerpecito de una pulga, la fuerza del elefante es difícilmente superable.</p>
                                            <p>Para que te hagas una idea de la magnitud de la lucha, según George Miller, la mente consciente (nuestra pulga) lanza un promedio de 20 a 40 disparos neuronales por segundo (corrientes de información que viajan por nuestro cuerpo para responder a los estímulos exteriores). La mente inconsciente (nuestro elefante) lanza de 20 a 40 millones de disparos por segundo, por lo que con este desequilibrio de artillería las opciones de victoria son muy limitadas.</p>
                                            <p>Las creencias limitantes viven agazapadas en los surcos profundos de nuestra mente inconsciente y se disparan a la velocidad de la luz cada vez que un acontecimiento externo (una palabra, un gesto, una persona, un comentario, un resultado, un comportamiento) las despierta. Porque, aunque habitan en las profundidades no están dormidas, sino en situación de vigilia permanente. </p>
                                            <p>En la mayor parte de los casos, ni sabemos cuándo se instalaron allí porque seguro fue hace mucho tiempo (de niños, en la adolescencia, incluso más mayores), no sabemos cómo se llaman y no sabemos qué las hace despertar. Son limitantes porque están en conflicto con nuestros deseos, objetivos, metas y valores conscientes.</p>
                                            <p>Supongamos que un día cuando tenías 7 años tu padre te prometió algo que para ti era muy importante y por algún motivo que no estaba en sus manos no cumplió. Y sí, hoy nos puede parecer una tontería y lo entendemos perfectamente desde nuestra mente racional, cuando tenías 7 años tu elefante, que ya era muy grande, lo vivió como una decepción, un abandono, una desilusión imperdonable de la persona en la que más confiabas. Y tu elefante con toda su fuerza decidió que «no se puede confiar en la gente porque tarde o temprano te fallan» y te lo creíste, porque quién es una pulga para contradecir a un elefante.</p>
                                            <p>Y ahora con muchos más años, ya eres líder de un proyecto en el que dependes de mucha gente, como todos los líderes, y te cargas de más y más trabajo que tienen que hacer otros. Y todos los días tu pulga te repite voy a delegar más, voy a exigir que cada uno asuma sus responsabilidades, pero tu elefante decide seguir sin delegar y haciendo el trabajo de los demás. Entonces, sientes que no disfrutas del trabajo porque además de la sobrecarga laboral, estás en situación de alerta permanente pensando en quién te va a fallar, y sometes a tu equipo a un control tan férreo que anulas su capacidad de pensar, de decidir, de actuar, contagiándoles el miedo a fallar, que realmente es tu miedo, el de tu elefante. Y un día, como no podía ser de otro modo, alguien del equipo no entrega algo a tiempo, el elefante se traga a la pulga, y desata toda su furia contra el mundo, la que quedó atrapada en ese recuerdo infantil con tu padre.</p>
                                            <p>Creencias limitantes: la lucha de la pulga y el elefante</p>
                                            <p>Así nace, se reproduce y actúa una creencia limitante. Así lo explican muy bien con la metáfora de la pulga y el elefante, los psicólogos George Pratt y Peter Lambrou en su libro El Código de la Felicidad. La mente inconsciente funciona principalmente por asociación, prestando atención a la resonancia emocional. Cuando tenemos una experiencia en el presente, nuestra mente consciente busca automáticamente experiencias del pasado que le resuenan a lo mismo. ¿Estás empezando a comprender lo que te pasa con algunas personas, tu actitud hacia ellas, tu comportamiento? ¿Cuánto es realidad presente y cuánto, suposición pasada?</p>
                                            
                                            <p>La asociación entre la experiencia presente y pasada pocas veces pasa al consciente, por lo que nuestra acción, nuestra respuesta al presente es elegida de forma inconsciente, porque como ya te he contado nuestro elefante es más rápido y fuerte que nuestra pulga. Y el problema es que muchas de nuestras experiencias pasadas se alojaron en nuestra mente inconsciente de una forma adulterada, no tal y como realmente sucedieron, sino tal y como las interpretamos nosotros, con la carga emocional y valorativa que les imprimimos.</p>
                                            
                                            <p>Y es esa misma emoción y carga valorativa del pasado la que derramamos sin consciencia en la experiencia presente, aunque sean dos situaciones totalmente distintas (no es el mismo tiempo, las mismas personas, el mismo contexto ni los mismos hechos). La descarga de ira contra el mundo por el fallo de un miembro del equipo, le ha ganado la batalla a la posibilidad de averiguar que pasó realmente para que se produjera el incumplimiento de entrega, qué parte de responsabilidad previa tienes tu sobrecargando con controles y desconfianza al equipo, si es un fallo insalvable o no, etc.</p>
                                            
                                            <p> Supongo que te estarás preguntando ¿qué puede hacer la pulga con el elefante? Te cuento algunas cosas:</p>
                                            
                                            <p>
                                                1.- Averigua en qué cree tu elefante. Descubre cuál es la creencia limitante por la que se guía tu elefante, por ejemplo: <br>
                                                No estoy a salvo, el mundo es peligroso. Ten cuidado. <br>
                                                No valgo nada o no valgo lo suficiente, no puedo conseguir lo que quiero, los demás son mejores, tengo que esforzarme por ser perfecto. <br>
                                                Estoy indefenso, las cosas escapan a mi control, no puedo hacer nada, soy una víctima. Necesito que me salven. <br>
                                                No merezco que me quieran, no soy lo bastante bueno por eso me abandonan. <br>
                                                No puedo confiar en nadie porque siempre me acaban fallando. <br>
                                                Soy torpe, hay algo en mí que no funciona, que no está bien. Estropeó las cosas. <br>
                                                Estoy solo, siento que no pertenezco a este mundo, que no me entienden, que soy distinto, que no conecto. <br>
                                                Indaga en acontecimientos pasados, aparentemente sin importancia, principalmente en tu infancia, que te resuenen emocionalmente a una de estas creencias. <br>
                                            </p>
                                            
                                            <p>
                                                2.- Intenta comprender a tu elefante, cómo se sintió, qué necesitaba y no tuvo, no le juzgues. Piensa ahora en un pasado más reciente, en situaciones que asocies con las creencias detectadas. Haz una nueva lectura de las mismas, volviendo a entender a tu elefante. A veces sencillos ejercicios de respiración pueden servirle a la pulga para calmar al elefante, para darle luego lo que necesita y que pueda caminar en la dirección que quiere la pulga.
                                            </p>
                                            
                                            <p>
                                                3.- Escribe en un papel cada una de tus creencias limitantes, como te hacen sentir que no te gusta, que te impulsan a hacer que no quieres. Léelo y si no quieres que siga formando parte de tu vida, estruja ese papel y tíralo a una palera. 
                                            </p>
                                            
                                            <p>
                                                4.- Visualiza como será tu vida si esas creencias limitantes, siéntelo y escucha lo que te dicen. Mantén esa imagen viva en tu mente. Asociarla a un símbolo, y si puedes haz un collage de imágenes con fotografías de revista para recordártela.
                                            </p>
                                            
                                            <p>
                                                5.- Por la noche, antes justo de apagar la luz para dormirte, escribe en una hoja o en una libreta, que tendrás siempre cerca en tu habitación, las nuevas creencias que quieres que formen parte de tu vida y que cuestionan y contradicen las que abandonaste en a la papelera.
                                            </p>
                                            
                                            <p>
                                                Por ejemplo, si tu creencia limitante es «soy torpe, algo falla en mi». Tu creencia potenciadora puede ser «aporto cosas buenas a todas las personas que conozco» y acompáñala de una lista con ejemplos concretos y claros de esas cosas buenas que aportas.
                                                Revisa esta lista todos los días al acostarte y al levantarte durante 30 días, y luego todas las veces que creas necesario, que dudes, que estés de bajón.
                                            </p>
                                            
                                            <p>
                                                Remover experiencias y creencias del pasado puede ser muy liberador si se está preparado para afrontarlo o gestionarlo, pero también puede abrir grietas de profundo dolor que necesitarán ayuda profesional para cerrarlas.
                                            </p>
                                            
                                            <p>
                                                Lo que si te puedo decir es que bien hecho y en el momento adecuado es liberador, gratificante y estimulante. Te da otra perspectiva de las cosas, te ayuda a crecer y te hace más fuerte y mejor.
                                            </p>
 


                                        </div>
                                        
                                    </div>
                                </article>

                                <div class="btn_regresar">
                                    <a href="blog.php" class="full"></a>
                                    Regresar
                                </div>
                               

                                
                                
                                
                            </div>
                            <div class="col-md-3">
                                <aside>
                                    <div class="listado_lateral_de_blogs">
                                        <div class="titulo naranja">
                                            Recientes
                                        </div>
                                        <ul class="lista">
                                            <li>
                                                <a href="blog-creencias-limitantes.php" class="full"></a>
                                                Creencias limitantes: la lucha de la pulga contra el elefante
                                            </li>
                                            <li>
                                                <a href="blog-plan-de-vida.php" class="full"></a>
                                                Plan de vida y de carrera
                                            </li>
                                            <li>
                                                <a href="blog-energia-vital.php" class="full"></a>
                                                Energia vital
                                            </li>
                                            <li>
                                                <a href="blog-resilencia.php" class="full"></a>
                                                Resiliencia
                                            </li>
                                        </ul>
                                    </div>
                                </aside>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>
 
    <script src="js/bootstrap-select.js"></script>     
    <script>
        function createOptions(number) {
            var options = [], _options;
            for (var i = 0; i < number; i++) {
                var option = '<option value="' + i + '">Asunto ' + i + '</option>';
                options.push(option);
            }
            _options = options.join('');          
            $('#selectAsunto')[0].innerHTML = _options;
        }
        createOptions(6);
    </script>
</body>
</html>