<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
     <?php include 'metas.html';?>

</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_chico" style="background-image: url(images/banner-blog.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            Blog
                        </h1>
                        <div class="subtitulo">
                            OMA reconoce que la innovación es un eje central <br>
                            para el fortalecimiento económico y social.
                        </div>
                    </div>
                </div>								
			</div>

            <section class="seccion_blog">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="blog.php" class="activo">Blog</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="lista_blog">
                    <div class="container">
                        <div class="row">   
                            <div class="col-md-9">
                                <h3>
                                    Mantente Actualizado
                                </h3>
                                <h2>
                                    Nuestro Blog y Noticias
                                </h2>
                                
                                <article class="blog_detalle">                                    
                                    <div class="imagen">
                                        <img src="images/blog-3.jpg">
                                    </div>
                                    <div class="b">
                                        <div class="tit">
                                            Plan de vida y de carrera
                                        </div>
                                        
                                        <div class="pie">
                                            <div class="autor">
                                                Energia vital
                                            </div>
                                            <div class="fecha">
                                                05/01/2022
                                            </div>
                                        </div>
                                        <div class="texto">
                                            <p>
                                                Nuestra energía vital proviene de cuatro fuentes: física, emocional, mental y espiritual. Nuestros rituales de energía deben contemplar la renovación de nuestra energía en estas cuatro dimensiones; solo si están balanceadas podemos extraer todo nuestro potencial. Aprender a usar la energía dentro de cada uno de esas cuatro dimensiones es la clave para conseguir nuestros objetivos.
                                            Los rituales energéticos son rutinas muy concretas y sencillas que nos permiten mantener nuestro caudal de energía equilibrado. Son comportamientos que apenas consumen energía. Estoy hablando de escuchar música todos los días nada más levantarse, darse baños de sol, pasear junto al mar, meditar, sentarse en un parque, desconectarse de la información y el ruido cada cierto tiempo, entre otros. Aprender a deshacernos de energías negativas, limpiarnos y renovarnos; saber qué nos recarga la energía y buscarlo nos ayudará a tener un bienestar y equilibrio vital que nos aportarán felicidad.
                                            Como seres humanos, contamos con una cantidad limitada de energía que malgastamos y nos dejamos robar por los ladrones energéticos que acechan nuestro día a día. Y digo, “nos dejamos robar” porque somos nosotros quienes decidimos sí o no ante su presencia, quienes les dejamos tomar las riendas, y quienes nos abandonamos en sus brazos como si no pudiéramos hacerles frente. Y luego no vale el “es que me dio pena”, “es que si no lo hago…”, “es que me tiene manía” y otros “es que” que no hacen más que reforzar a esos ladrones energéticos. 

                                            <p>
                                                Estos son algunos de ellos: <br>
                                            Enfadarnos al tomarnos las cosas de forma personal. 
                                            Actuar guiados por el “tengo que” en vez de por el “quiero”. 
                                            Cuando nuestro ego le gana la partida a nuestra esencia, toma el mando de la situación y tomamos decisiones para satisfacerlo en contra de nuestra esencia. </p>

                                            <p>
                                                Cuando realizamos acciones que nos alejan de nuestras metas. 
                                            Las quejas incesantes que emitimos y que escuchamos. 
                                            Los chismes y rumores. </p>

                                            <p>
                                                El tráfico mental, ese run run incesante de recordatorios de tareas que tenemos que hacer, que mientras nos obligamos a repasarlas continuamente para no olvidarlas, perdemos el tiempo para dejarlas solucionadas. 
                                            La indecisión permanente, que hace que se instalen en nuestra mente, duda tras duda, imaginando probabilidades de situaciones que nunca se darán. Permanecer en la inacción consume energía porque la acción revitaliza y moviliza la energía. 
                                            Los vampiros emocionales, esas personas negativas que nos chupan nuestra energía. Son esas víctimas a quien siempre ayudar, a quien siempre salvar, que manejan nuestros sentimientos de culpa, de pena. Al final, dedicamos nuestro tiempo a sus problemas olvidándonos de los nuestros y eso va desgastando nuestra energía, generando emociones de ira, tristeza e incluso asco. 
                                            Cuando actuamos buscando la aprobación o aceptación de los demás y dejamos de ser nosotros mismos.
                                            Cuando nos juzgamos y rechazamos por no responder al ideal de perfección que nos marcaron.
                                            Rompe con todo eso, aléjate de ello, y dedícate a ser impecable personalmente, solo así podrás desarrollar todo tu potencial. Para ello, comienza por ser impecable con tus palabras, las que te dices a ti mismo y las que les dices a los demás. A través de las palabras comienza a fluir la energía o se bloquea. Piensa simplemente en el estado interior que se genera dentro de ti al decir “puedo hacerlo” frente a decir “no puedo hacerlo”. Ahora ya sabes cómo las palabras que pronuncias influyen en tu energía.
                                            Practica una generosidad inteligente. Cuando das demasiado a los demás te quedas vacío, cuando te dedicas a dar a quien no te da o no te aporta nada te acabas irritando. Ambos comportamientos te sobrecargan de energía negativa y te desequilibran. La impecabilidad personal implica un equilibrio entre el dar y recibir, y rodearse de personas que practican este mismo hábito.
                                            Enfoca y gestiona bien tu pasión. Si concentras toda tu energía en lograr cosas que realmente sólo satisfacen a tu ego, a su afán por figurar, por demostrar, por conservar su estatus, o bien para ser aceptado o pertenecer a un grupo, tu pasión se convertirá en obsesiva y será la principal vía de escape de tu energía. La impecabilidad personal nace desde el “quiero” y la esencia, y no desde el “tengo” y el ego.</p>

                                            <p>
                                                No te abandones en los brazos de un optimismo sin medida porque te hará consumir cada vez más energía para poder ver la realidad de un color distinto al que es, para justificar tus fracasos y volverlo a intentar probablemente cometiendo los mismos errores. Vivir de espaldas a la realidad consume tanta energía como verla siempre de color negro. La impecabilidad personal está aliada siempre con el optimismo realista, un optimismo que es flexible y sabe cuándo dar espacio al pesimismo y cuándo dejarlo atrás.
                                            </p>

                                            <p>
                                                La impecabilidad personal implica poner lo mejor de sí mismo en cada acto, en cada acción, y esto solo es posible con un equilibrio de cuerpo, emoción, mente y espíritu. Con un vivir congruente entre lo que somos, lo que sentimos, lo que pensamos, y lo que expresamos. Solo cuando existe este equilibrio y congruencia nuestra energía vibra positivamente y podemos resonar positivamente produciendo un efecto contagio en los demás y en el ambiente.
                                            </p>



                                        </div>
                                        
                                    </div>
                                </article>

                                <div class="btn_regresar">
                                    <a href="blog.php" class="full"></a>
                                    Regresar
                                </div>
                               

                                
                                
                                
                            </div>
                            <div class="col-md-3">
                                <aside>
                                    <div class="listado_lateral_de_blogs">
                                        <div class="titulo naranja">
                                            Recientes
                                        </div>
                                        <ul class="lista">
                                            <li>
                                                <a href="blog-creencias-limitantes.php" class="full"></a>
                                                Creencias limitantes: la lucha de la pulga contra el elefante
                                            </li>
                                            <li>
                                                <a href="blog-plan-de-vida.php" class="full"></a>
                                                Plan de vida y de carrera
                                            </li>
                                            <li>
                                                <a href="blog-energia-vital.php" class="full"></a>
                                                Energia vital
                                            </li>
                                            <li>
                                                <a href="blog-resilencia.php" class="full"></a>
                                                Resiliencia
                                            </li>
                                        </ul>
                                    </div>
                                </aside>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>
 
    <script src="js/bootstrap-select.js"></script>     
    <script>
        function createOptions(number) {
            var options = [], _options;
            for (var i = 0; i < number; i++) {
                var option = '<option value="' + i + '">Asunto ' + i + '</option>';
                options.push(option);
            }
            _options = options.join('');          
            $('#selectAsunto')[0].innerHTML = _options;
        }
        createOptions(6);
    </script>
</body>
</html>