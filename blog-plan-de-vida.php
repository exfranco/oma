<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
     <?php include 'metas.html';?>

</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_chico" style="background-image: url(images/banner-blog.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            Blog
                        </h1>
                        <div class="subtitulo">
                            OMA reconoce que la innovación es un eje central <br>
                            para el fortalecimiento económico y social.
                        </div>
                    </div>
                </div>								
			</div>

            <section class="seccion_blog">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="blog.php" class="activo">Blog</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="lista_blog">
                    <div class="container">
                        <div class="row">   
                            <div class="col-md-9">
                                <h3>
                                    Mantente Actualizado
                                </h3>
                                <h2>
                                    Nuestro Blog y Noticias
                                </h2>
                                
                                <article class="blog_detalle">                                    
                                    <div class="imagen">
                                        <img src="images/blog-2.jpg">
                                    </div>
                                    <div class="b">
                                        <div class="tit">
                                            Plan de vida y de carrera
                                        </div>
                                        
                                        <div class="pie">
                                            <div class="autor">
                                               
                                            </div>
                                            <div class="fecha">
                                                
                                            </div>
                                        </div>
                                        <div class="texto">
                                            <p>Planteamiento <br>
                                            Un plan de vida supone la enumeración de los objetivos que una persona quiere lograr a lo largo de su vida y una guía que propone cómo alcanzarlos. Este plan puede incluir metas personales, profesionales, económicas y espirituales.</p>
                                            
                                            <p>El plan de vida incluye objetivos a largo plazo: la persona puede pensar dónde le gustaría estar dentro de cinco o diez años, y a partir de esa idea, comenzar a desarrollar el plan. En este sentido, el plan de vida también es un plan de acción, con pasos a seguir y plazos, una estructura que permite encausar las acciones hacia las metas que una persona desea cumplir en sus años de existencia. <br>
                                            Tener un PVC es importante porque denota el liderazgo y voluntad de la persona de hacer que las cosas sucedan. </p>
                                            
                                            <p>Un Plan de Vida y Carrera es la actitud, arte y disciplina de conocerse así mismo, de detectar tanto las fortalezas como debilidades propias y del medio que nos rodea, para proyectar nuestro propio destino, autodirigiéndolo hacia el pleno funcionamiento de las capacidades, motivaciones y objetivos de la vida profesional, personal y de trabajo.</p>
                                            
                                            <p>Al igual que cualquier plan, éste tiene que ser analizado de manera periódica, de modo tal que la persona pueda advertir si se acerca al cumplimiento de sus objetivos o no. En caso de que las acciones realizadas no rindan sus frutos, el individuo está en condiciones de rectificarlas o de proponer nuevos caminos.</p>
                                            
                                            <p>Los psicólogos han comparado el plan de vida con el plan de negocios en cuanto a que un negocio o emprendimiento comercial tiene sentido siempre que permita a su impulsor satisfacer ciertas necesidades personales. Y entre ellas se encuentra la alimentación, que es la base de nuestra supervivencia, al menos desde un punto de vista meramente fisiológico. La alimentación es un aspecto de la vida tan básico que a menudo nadie se detiene a pensar en todo lo que implica nuestra forma de llevarla a cabo.</p>
                                            
                                            <p>Así como apoyar el arte conlleva entender el trabajo que hay detrás de un cuadro o de una obra de teatro, modificar la propia alimentación acarrea desaprobar las costumbres que se dejan atrás.</p>
                                            
                                            <p>Otro punto muy importante de la vida es la profesión, que puede estar íntimamente ligada a la vocación, o bien a la ausencia de la misma. Algunos más tarde que otros, pero probablemente todos nos preguntemos en cierto momento de nuestra existencia “qué queremos hacer con ella”, “a qué queremos dedicarnos”. Esto abre una serie muy amplia de caminos posibles, con dos tendencias seguramente muy marcadas: por un lado, la búsqueda de la felicidad y del desarrollo personal, y por el otro, las ambiciones económicas. Muchas personas escogen una carrera para no decepcionar a sus padres, otras deciden elaborar una estrategia revolucionaria de negocios para volverse ricas, y algunas se conforman con la satisfacción de compartir sus creaciones artísticas con su entorno, entre una cantidad potencialmente infinita de personalidades. <br>                                            
                                            Por último, el aspecto sentimental ocupa mucho tiempo y energías de la mayoría de las personas. Si bien el éxito del matrimonio ha descendido notablemente en los últimos años, muchos detractores de este lazo simbólico viven atormentados ante la mera idea de quedarse solteros, y planean sus vidas en torno a una potencial convivencia con otro ser humano.</p>
                                            
                                            <p>¿Cómo elaboró un Plan de Vida y Carrera?</p>
                                            
                                            <p>1. Analizar los antecedentes
                                            ¿Cuál es mi situación? ¿Cuáles son mis fortalezas y debilidades? Éstas u otras preguntas propiciarán una reflexión. No se trata de elaborar un juicio, sino de hacer una introspección.</p>
                                            
                                            <p>2. Establecer metas
                                            Una meta se piensa tomando en cuenta el contexto personal y lo que se desea. Debe redactarse en forma clara para que sea útil en la toma de decisiones. Además, es necesario considerar metas del aspecto social, por ejemplo: ¿cómo puedo ayudar a la comunidad?</p>
                                            
                                            <p>3. Fijar las estrategias o acciones
                                            Las acciones deben ser claras, con secuencia lógica.</p>
                                            
                                            <p>4. Determinar un indicador
                                            Una meta sin un indicador es sólo un buen deseo. Por ello, es necesario establecer un proceso para evaluar cómo va el avance.</p>
                                            
                                            <p>Un Plan de Vida y Carrera bien estructurado permite tomar una decisión 
                                            ante una oportunidad.</p>


                                        </div>
                                        
                                    </div>
                                </article>

                                <div class="btn_regresar">
                                    <a href="blog.php" class="full"></a>
                                    Regresar
                                </div>
                               

                                
                                
                                
                            </div>
                            <div class="col-md-3">
                                <aside>
                                    <div class="listado_lateral_de_blogs">
                                        <div class="titulo naranja">
                                            Recientes
                                        </div>
                                        <ul class="lista">
                                            <li>
                                                <a href="blog-creencias-limitantes.php" class="full"></a>
                                                Creencias limitantes: la lucha de la pulga contra el elefante
                                            </li>
                                            <li>
                                                <a href="blog-plan-de-vida.php" class="full"></a>
                                                Plan de vida y de carrera
                                            </li>
                                            <li>
                                                <a href="blog-energia-vital.php" class="full"></a>
                                                Energia vital
                                            </li>
                                            <li>
                                                <a href="blog-resilencia.php" class="full"></a>
                                                Resiliencia
                                            </li>
                                        </ul>
                                    </div>
                                </aside>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>
 
    <script src="js/bootstrap-select.js"></script>     
    <script>
        function createOptions(number) {
            var options = [], _options;
            for (var i = 0; i < number; i++) {
                var option = '<option value="' + i + '">Asunto ' + i + '</option>';
                options.push(option);
            }
            _options = options.join('');          
            $('#selectAsunto')[0].innerHTML = _options;
        }
        createOptions(6);
    </script>
</body>
</html>