<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
     <?php include 'metas.html';?>

</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_chico" style="background-image: url(images/banner-blog.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            Blog
                        </h1>
                        <div class="subtitulo">
                            OMA reconoce que la innovación es un eje central <br>
                            para el fortalecimiento económico y social.
                        </div>
                    </div>
                </div>								
			</div>

            <section class="seccion_blog">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="blog.php" class="activo">Blog</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="lista_blog">
                    <div class="container">
                        <div class="row">   
                            <div class="col-md-9">
                                <h3>
                                    Mantente Actualizado
                                </h3>
                                <h2>
                                    Nuestro Blog y Noticias
                                </h2>
                                
                                <article class="blog_detalle">                                    
                                    <div class="imagen">
                                        <img src="images/blog-4.jpg">
                                    </div>
                                    <div class="b">
                                        <div class="tit">
                                            RESILIENCIA
                                        </div>
                                        
                                        <div class="pie">
                                            <div class="autor">
                                                Tomado de Guillermo Quiroga, artículo en SE
                                            </div>
                                            <div class="fecha">
                                                05/01/2022
                                            </div>
                                        </div>
                                        <div class="texto">
                                            <p>Todos tenemos un plan hasta que nos cae un primer golpe.  Hoy, todos, en mayor o menor medida, hemos recibido un tremendo golpe.</p>
                                            <p>La crisis por el Covid 19 nos ha hecho repensar nuestros caminos No importa quienes seamos, las cosas malas nos pasan a todos. Independientemente de la precaución o del dinero. La realidad nos hace saber que no podemos controlar el viento, pero sí alinear las velas.</p>
                                            <p>Por eso ahora la competencia más útil es la resiliencia.  La resiliencia es la velocidad y la fuerza de respuesta ante la adversidad. Frente al desafío, ¿qué tan rápido y efectivos somos para acumular fuerza y superarlo y perseverar? Lo más importante de la resiliencia es que se puede ejercitar y por lo tanto desarrollar. Los humanos somos resilientes: la historia lo demuestra. </p>
                                            <p>Para fortalecer nuestra resiliencia podemos trabajar algunas actitudes:</p>

                                            <ul class="lista_check_orange">
                                                <li><p><strong>Perspectiva y foco en el largo plazo.</strong>  Ver siempre un túnel y no un pozo.  Normalmente hay salida, por ello debemos ser positivos, adaptarnos y buscar nuevas oportunidades o salidas.  Tomemos de ejemplo el caso Zoom que hoy usamos para todo y que agradecemos sobre manera. A su fundador Eric Yuan le rechazaron la visa a EE.UU. 8 veces. Cuando presentó su idea no le hicieron caso porque ya había otros productos similares. Tuvo que renunciar y fundar su propia empresa y le fue muy difícil conseguir financiamiento, hasta que lo logró.</p></li>
                                                <li><p><strong>Pasión por lo que se hace.</strong> Por más difícil que sea, debemos disfrutar la tarea.  Siguiendo con la historia de Zoom, Yuan comentaba: será un viaje muy largo y duro, pero me arrepentiré si no lo intento.</p></li>
                                                <li><p><strong>Manejo de la ansiedad y el miedo, para no estresarse, agotarse ni paralizarse.</strong> Celebrar intensamente lo bueno y tener calma ante lo malo.</p></li>
                                                <li><p><strong>Generar de conversaciones transparentes e íntegras.</strong> Dan confianza. Las armas clave son la inteligencia emocional y el aprendizaje continuo. El fracaso será un tutorial sobre la preparación y la perseverancia. Debemos tratar los sentimientos que nos molestan como momentos de aprendizaje; el arrepentimiento enseña a tomar mejores decisiones, y el sentimiento de culpa es una lección para hacer lo correcto más adelante.   Ante la rabia, el autocontrol. </p></li>
                                            </ul>


                                            <p>Todo lo anterior se resume en el aprendizaje continuo. Es decir, actividades que aumenten el conocimiento y mejoren nuestras competencias personales, además de la preparación académica. Ante un mundo en disrupción, donde los conocimientos y las habilidades caducan, es vital nunca dejar de aprender. </p>

                                            <p><strong>Longanimidad (tomado de Alex Rovira)</strong></p>

                                            <p>Las palabras cuando no se utilizan desaparecen del lenguaje cotidiano. La longanimidad es una de ellas; es sinónimo de magnanimidad, que es una palabra que viene del latín y significa alma extensa. Quiere decir la perseverancia y constancia de ánimo en situaciones adversas.  </p>

                                            <p>Gracias al trabajo del psicólogo Boris Cyrulnik que de niño sobrevivió a un campo nazi, se conoce el término resiliencia, sobre todo a partir de su obra “Los patitos feos”. La palabra resiliencia se deriva de la física.  Es la capacidad que tienen los cuerpos de recuperar su forma original tras haber sido sometidos a una fuerte presión. Un plástico, por ejemplo, es resiliente cuando lo sueltas muchas veces se endereza. La resiliencia también hace referencia al remanente eléctrico; hay bombillas de led que apenas las apagas siguen proporcionando luminiscencia durante un cierto rato. </p>

                                            <p>Pero la longanimidad es la resiliencia sostenida en el tiempo, en realidad hace referencia a la fuerza de ánimo para superar reiteradamente situaciones de adversidad. La segunda definición de Longanimidad es muy bonita, porque dice que la persona con longanimidad tiene benignidad, clemencia y generosidad. Y no es difícil llegar a la siguiente conclusión: para tener esa fuerza interior que combina perseverancia, paciencia y entrega hay que tener voluntad de amar, hay que tener voluntad de empatía.  Sin duda, eso es lo que la vida nos va a pedir a partir de ahora. Creo que vamos a oír y sería muy bonito oír que una persona es longánime. Por ejemplo, las personas que trabajan en el sistema sanitario cada día entregándose, durante horas y dándolo todo para sanar a nuestros enfermos. La persona longánime es una persona generosa, entregada, sacrificada fiel y leal; vive para dar con un enorme corazón que vive para servir. Sin duda la transformación de la vida depende de poner en acción lo mejor del ser humano y la longanimidad es una palabra que no deberíamos olvidar y qué bonito sería poder decirle a alguien admiro tu longanimidad. Ese es el mensaje, no puede haber buena suerte colectiva sin longanimidad. Es una virtud, un ultra valor, solo quien tiene constancia de ánimo y benignidad. Creemos una pandemia de longanimidad.</p>




                                        </div>
                                        
                                    </div>
                                </article>

                                <div class="btn_regresar">
                                    <a href="blog.php" class="full"></a>
                                    Regresar
                                </div>
                               

                                
                                
                                
                            </div>
                            <div class="col-md-3">
                                <aside>
                                    <div class="listado_lateral_de_blogs">
                                        <div class="titulo naranja">
                                            Recientes
                                        </div>
                                        <ul class="lista">
                                            <li>
                                                <a href="blog-creencias-limitantes.php" class="full"></a>
                                                Creencias limitantes: la lucha de la pulga contra el elefante
                                            </li>
                                            <li>
                                                <a href="blog-plan-de-vida.php" class="full"></a>
                                                Plan de vida y de carrera
                                            </li>
                                            <li>
                                                <a href="blog-energia-vital.php" class="full"></a>
                                                Energia vital
                                            </li>
                                            <li>
                                                <a href="blog-resilencia.php" class="full"></a>
                                                Resiliencia
                                            </li>
                                        </ul>
                                    </div>
                                </aside>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>
 
    <script src="js/bootstrap-select.js"></script>     
    <script>
        function createOptions(number) {
            var options = [], _options;
            for (var i = 0; i < number; i++) {
                var option = '<option value="' + i + '">Asunto ' + i + '</option>';
                options.push(option);
            }
            _options = options.join('');          
            $('#selectAsunto')[0].innerHTML = _options;
        }
        createOptions(6);
    </script>
</body>
</html>