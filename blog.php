<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
     <?php include 'metas.html';?>

</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_chico"  style="background-image: url(images/banner-blog.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            Blog
                        </h1>
                        <div class="subtitulo">
                            OMA reconoce que la innovación es un eje central <br>
                            para el fortalecimiento económico y social.
                        </div>
                    </div>
                </div>								
			</div>

            <section class="seccion_blog">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="blog.php" class="activo">Blog</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="lista_blog">
                    <div class="container">
                        <div class="row">   
                            <div class="col-md-9">
                                <h3>
                                    Mantente Actualizado
                                </h3>
                                <h2>
                                    Nuestro Blog y Noticias
                                </h2>

                                
                                <article>
                                    <a href="blog-creencias-limitantes.php" class="full"></a>
                                    <div class="imagen">
                                        <img src="images/blog-1.jpg">
                                    </div>
                                    <div class="b">
                                        <div class="tit">
                                            Creencias limitantes: la lucha de la pulga contra el elefante
                                        </div>
                                        
                                        <div class="pie">
                                            <div class="autor">
                                                María Luisa de Miguel 
                                            </div>
                                            <div class="fecha">
                                                05/01/2022
                                            </div>
                                        </div>
                                        <div class="texto">
                                            ¿Te imaginas una lucha entre una pulga y un elefante? Sí, aparte de ser una batalla perdida que no te llevará a ganar ninguna guerra, es una lucha agotadora y desgastante. La misma lucha que todos los días peleamos, sin ser conscientes de ello, entre lo que realmente queremos conseguir y lo que nuestras creencias limitantes nos dejan.
                                        </div>
                                        <div class="btn_leer">
                                            Leer más ...>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <a href="blog-plan-de-vida.php" class="full"></a>
                                    <div class="imagen">
                                        <img src="images/blog-2.jpg">
                                    </div>
                                    <div class="b">
                                        <div class="tit">
                                            Plan de vida y de carrera
                                        </div>
                                        
                                        <div class="pie">
                                            <div class="autor">
                                                
                                            </div>
                                            <div class="fecha">
                                                
                                            </div>
                                        </div>
                                        <div class="texto">
                                            Planteamiento <br>
                                            Un plan de vida supone la enumeración de los objetivos que una persona quiere lograr a lo largo de su vida y una guía que propone cómo alcanzarlos. Este plan puede incluir metas personales, profesionales, económicas y espirituales.
                                        </div>
                                        <div class="btn_leer">
                                            Leer más ...>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <a href="blog-energia-vital.php" class="full"></a>
                                    <div class="imagen">
                                        <img src="images/blog-3.jpg">
                                    </div>
                                    <div class="b">
                                        <div class="tit">
                                            Energia vital
                                        </div>
                                        
                                        <div class="pie">
                                            <div class="autor">
                                                
                                            </div>
                                            <div class="fecha">
                                                
                                            </div>
                                        </div>
                                        <div class="texto">
                                            Nuestra energía vital proviene de cuatro fuentes: física, emocional, mental y espiritual. Nuestros rituales de energía deben contemplar la renovación de nuestra energía en estas cuatro dimensiones; solo si están balanceadas podemos extraer todo nuestro potencial. Aprender a usar la energía dentro de cada uno de esas cuatro dimensiones es la clave para conseguir nuestros objetivos.
                                        </div>
                                        <div class="btn_leer">
                                            Leer más ...>
                                        </div>
                                    </div>
                                </article>
                                <article>
                                    <a href="blog-resilencia.php" class="full"></a>
                                    <div class="imagen">
                                        <img src="images/blog-4.jpg">
                                    </div>
                                    <div class="b">
                                        <div class="tit">
                                            RESILIENCIA
                                        </div>
                                        
                                        <div class="pie">
                                            <div class="autor">
                                                Tomado de Guillermo Quiroga, artículo en SE
                                            </div>
                                            <div class="fecha">
                                                05/01/2022
                                            </div>
                                        </div>
                                        <div class="texto">
                                            Todos tenemos un plan hasta que nos cae un primer golpe.  Hoy, todos, en mayor o menor medida, hemos recibido un tremendo golpe. La crisis por el Covid 19 nos ha hecho repensar nuestros caminos No importa quienes seamos, las cosas malas nos pasan a todos. Independientemente de la precaución o del dinero. La realidad nos hace saber que no podemos controlar el viento, pero sí alinear las velas.
                                        </div>
                                        <div class="btn_leer">
                                            Leer más ...>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="col-md-3">
                                <aside>
                                    <div class="listado_lateral_de_blogs">
                                        <div class="titulo naranja">
                                            Recientes
                                        </div>
                                        <ul class="lista">
                                            <li>
                                                <a href="blog-creencias-limitantes.php" class="full"></a>
                                                Creencias limitantes: la lucha de la pulga contra el elefante
                                            </li>
                                            <li>
                                                <a href="blog-plan-de-vida.php" class="full"></a>
                                                Plan de vida y de carrera
                                            </li>
                                            <li>
                                                <a href="blog-energia-vital.php" class="full"></a>
                                                Energia vital
                                            </li>
                                            <li>
                                                <a href="blog-resilencia.php" class="full"></a>
                                                Resiliencia
                                            </li>
                                        </ul>
                                    </div>
                                </aside>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>
 
    <script src="js/bootstrap-select.js"></script>     
    <script>
        function createOptions(number) {
            var options = [], _options;
            for (var i = 0; i < number; i++) {
                var option = '<option value="' + i + '">Asunto ' + i + '</option>';
                options.push(option);
            }
            _options = options.join('');          
            $('#selectAsunto')[0].innerHTML = _options;
        }
        createOptions(6);
    </script>
</body>
</html>