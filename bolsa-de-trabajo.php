<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-bolsa.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            <span>ASOCIACIÓN OMA</span> <br>
                            BOLSA DE TRABAJO
                        </h1>
                        <div class="subtitulo">
                            El talento está formado por la suma de conocimientos, <br>
                            competencias blandas, ética, compromiso y acción.
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="bolsa-de-trabajo.php" class="activo">Bolsa de trabajo</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_bolsa">
                    <div class="container">
                        <div class="row">
                           
                            <div class="col-12">
                                <h2>
                                    Bolsa de trabajo
                                </h2>
                                <div class="lista_standar">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-3">
                                            <article>
                                                <a href="#" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/bolsa-logo-1.jpg">
                                                </div>
                                                <div class="tit">
                                                    Ingeniero
                                                </div>
                                                <div class="texto">
                                                    <p>
                                                        Fecha: <br>
                                                        Departamento:
                                                    </p>
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                                                </div>

                                                <div class="btn_info">
                                                    Mas Información
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-3">
                                            <article>
                                                <a href="#" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/bolsa-logo-2.jpg">
                                                </div>
                                                <div class="tit">
                                                    Ingeniero
                                                </div>
                                                <div class="texto">
                                                    <p>
                                                        Fecha: <br>
                                                        Departamento:
                                                    </p>
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                                                </div>

                                                <div class="btn_info">
                                                    Mas Información
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-3">
                                            <article>
                                                <a href="#" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/bolsa-logo-3.jpg">
                                                </div>
                                                <div class="tit">
                                                    Ingeniero
                                                </div>
                                                <div class="texto">
                                                    <p>
                                                        Fecha: <br>
                                                        Departamento:
                                                    </p>
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                                                </div>

                                                <div class="btn_info">
                                                    Mas Información
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-3">
                                            <article>
                                                <a href="#" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/bolsa-logo-4.jpg">
                                                </div>
                                                <div class="tit">
                                                    Ingeniero
                                                </div>
                                                <div class="texto">
                                                    <p>
                                                        Fecha: <br>
                                                        Departamento:
                                                    </p>
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                                                </div>

                                                <div class="btn_info">
                                                    Mas Información
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-3">
                                            <article>
                                                <a href="#" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/bolsa-logo-5.jpg">
                                                </div>
                                                <div class="tit">
                                                    Ingeniero
                                                </div>
                                                <div class="texto">
                                                    <p>
                                                        Fecha: <br>
                                                        Departamento:
                                                    </p>
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                                                </div>

                                                <div class="btn_info">
                                                    Mas Información
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>