<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-compromiso.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                           Conócenos
                        </h1>
                        <div class="subtitulo">
                            Nuestro motor es el brillo de esperanza en <br> la mirada de nuestros estudiantes
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_conocenos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="conocenos-quienes-somos.php">Conócenos</a></li>
                            <li>/ <a href="conocenos-nuestro-compromiso.php" class="activo">Nuestro Compromiso</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_nuestro_compromiso">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3 order-md-1"><div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral">
                                    <li>
                                        <a href="conocenos-quienes-somos.php" class="full"></a>
                                        ¿Quiénes Somos?
                                    </li>
                                    <li>
                                        <a href="conocenos-objetivos.php" class="full"></a>
                                        Objetivos
                                    </li>
                                    <li class="activo">
                                        <a href="conocenos-nuestro-compromiso.php" class="full"></a>
                                        Nuestro Compromiso
                                    </li>
                                    <li>
                                        <a href="conocenos-nuestro-equipo.php" class="full"></a>
                                        Nuestro Equipo
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2>
                                Nuestro Compromiso
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="row">
                                    <div class="col-md-7">
                                        <ul class="lista_check_orange lista_margen">
                                            <li>                                                
                                                Formar estudiantes líderes y emprendedores.                                                
                                            </li>
                                            <li>                                                
                                                Proveer herramientas para el éxito personal.                                                
                                            </li>
                                            <li>                                                
                                                Capacitar estudiantes universitarios y docentes mediante talleres interactivos y charlas especializadas.                                                
                                            </li>
                                            <li>                                                
                                                Formar equipos de trabajo con las comunidades para apoyar a la minería responsable promoviendo la innovación y conservación del medio ambiente.                                                
                                            </li>
                                            <li>                                                
                                                Crear alianzas estratégicas con asociaciones y entidades educativas.                                                
                                            </li>
                                            <li>                                                
                                                Convocar a la empresa privada a unirse en este esfuerzo de colaboración y gestión de cambio.                                                
                                            </li>
                                            <li>                                                
                                                Crear cultura equitativa e inclusiva económicamente gracias a la minería y al agro.                                                
                                            </li>
                                            <li>                                                
                                                Apoyar el desarrollo sostenible para satisfacer las necesidades del presente sin poner en peligro la capacidad de las generaciones futuras de atender sus propias necesidades.                                                
                                            </li> 
                                        </ul>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="foto">
                                            <img src="images/conocenos-nuestro-compromiso-1.jpg">
                                        </div>
                                        <div class="foto">
                                            <img src="images/conocenos-nuestro-compromiso-2.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>

                <?php include 'seccion_testimonios.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>