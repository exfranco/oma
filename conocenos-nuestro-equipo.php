<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-nuestro-equipo.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                           Conócenos
                        </h1>
                        <div class="subtitulo">
                            Organización Mundial de Apoyo a la Educación
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_conocenos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="conocenos-quienes-somos.php">Conócenos</a></li>
                            <li>/ <a href="conocenos-nuestro-equipo.php" class="activo">Nuestro Equipo</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_nuestro_equipo">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3 order-md-1"><div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral">
                                    <li>
                                        <a href="conocenos-quienes-somos.php" class="full"></a>
                                        ¿Quiénes Somos?
                                    </li>
                                    <li>
                                        <a href="conocenos-objetivos.php" class="full"></a>
                                        Objetivos
                                    </li>
                                    <li>
                                        <a href="conocenos-nuestro-compromiso.php" class="full"></a>
                                        Nuestro Compromiso
                                    </li>
                                    <li class="activo">
                                        <a href="conocenos-nuestro-equipo.php" class="full"></a>
                                        Nuestro Equipo
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2>
                                Nuestro Equipo
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="lista_nuestro_equipo">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <div class="imagen">
                                                    <img src="images/nuestro-equipo-1.jpg">
                                                </div>
                                                <div class="name">
                                                    Pilar Benavides Alfaro
                                                </div>
                                                <div class="cargo">
                                                    Presidente
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <div class="imagen">
                                                    <img src="images/nuestro-equipo-2.jpg">
                                                </div>
                                                <div class="name">
                                                    Susana Watson
                                                </div>
                                                <div class="cargo">
                                                    Vicepresidente
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <div class="imagen">
                                                    <img src="images/nuestro-equipo-3.jpg">
                                                </div>
                                                <div class="name">
                                                    Nicolangelo Pelosi
                                                </div>
                                                <div class="cargo">
                                                    Asesor externo
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <div class="imagen">
                                                    <img src="images/nuestro-equipo-4.jpg">
                                                </div>
                                                <div class="name">
                                                    Claudia Beltrán
                                                </div>
                                                <div class="cargo">
                                                    Directora del programa de coaching y directora del programa Mujeres Roca en OMA
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <div class="imagen">
                                                    <img src="images/nuestro-equipo-5.jpg">
                                                </div>
                                                <div class="name">
                                                    Pilar León
                                                </div>
                                                <div class="cargo">
                                                    Socia fundadora
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <div class="imagen">
                                                    <img src="images/nuestro-equipo-6.jpg">
                                                </div>
                                                <div class="name">
                                                    Mónica León
                                                </div>
                                                <div class="cargo">
                                                    Socia fundadora
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <div class="imagen">
                                                    <img src="images/nuestro-equipo-7.jpg">
                                                </div>
                                                <div class="name">
                                                    Ever Chuquisengo
                                                </div>
                                                <div class="cargo">
                                                    Coordinador General
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <div class="imagen">
                                                    <img src="images/nuestro-equipo-8.jpg">
                                                </div>
                                                <div class="name">
                                                    Antuanet Boteri
                                                </div>
                                                <div class="cargo">
                                                    Coordinadora
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <div class="imagen">
                                                    <img src="images/nuestro-equipo-9.jpg">
                                                </div>
                                                <div class="name">
                                                    William Canales
                                                </div>
                                                <div class="cargo">
                                                    Tesorero
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>

                <?php include 'seccion_testimonios.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>