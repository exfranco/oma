<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-objetivos.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                           Conócenos
                        </h1>
                        <div class="subtitulo">
                            Organización Mundial de Apoyo a la Educación
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_conocenos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="conocenos-quienes-somos.php">Conócenos</a></li>
                            <li>/ <a href="conocenos-objetivos.php" class="activo">Objetivos</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_objetivos">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3 order-md-1"><div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral">
                                    <li>
                                        <a href="conocenos-quienes-somos.php" class="full"></a>
                                        ¿Quiénes Somos?
                                    </li>
                                    <li class="activo">
                                        <a href="conocenos-objetivos.php" class="full"></a>
                                        Objetivos
                                    </li>
                                    <li>
                                        <a href="conocenos-nuestro-compromiso.php" class="full"></a>
                                        Nuestro Compromiso
                                    </li>
                                    <li>
                                        <a href="conocenos-nuestro-equipo.php" class="full"></a>
                                        Nuestro Equipo
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2>
                                Objetivos
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="texto">
                                            <p><span>Impulsar la descentralización del país llevando</span> nuestros talleres a todas las regiones del Perú para ayudar a reducir la brecha competitiva entre los estudiantes de provincia y los de la capital.</p>
                                            <p><span>Contribuir al bienestar de las comunidades</span> reforzando la autoestima, identidad y competencias personales de sus jóvenes.</p>
                                            <p><span>Generar valor para las organizaciones y empresas</span> al promover la formación de profesionales responsables, productivos e innovadores.</p>
                                            <p><span>Avanzar hacia un desarrollo sostenible al contribuir a la disminución de la pobreza</span> por medio del apoyo a la educación y a la enseñanza de valores humanos con respeto a la diversidad y al medio ambiente.</p>
                                            <p><span>Estimular en cada estudiante un mayor compromiso con su desarrollo,</span> fortaleciendo sus competencias humanas y valores mediante talleres de capacitación virtuales y presenciales, mentorías y charlas semanales.  </p>
                                            <p><span>Desarrollar competencias para una efectiva inserción universitaria y laboral.</span></p>
 
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="foto">
                                            <img src="images/conocenos-objetivos-1.jpg">
                                        </div>
                                        <div class="foto">
                                            <img src="images/conocenos-objetivos-2.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>

                <?php include 'seccion_testimonios.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>