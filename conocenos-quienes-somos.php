<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-conocenos.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                           Conócenos
                        </h1>
                        <div class="subtitulo">
                            Organización Mundial de Apoyo a la Educación
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_conocenos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="conocenos-quienes-somos.php">Conócenos</a></li>
                            <li>/ <a href="conocenos-quienes-somos.php" class="activo">¿Quiénes Somos?</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_quienes_somos">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3 order-md-1"><div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral">
                                    <li class="activo">
                                        <a href="conocenos-quienes-somos.php" class="full"></a>
                                        ¿Quiénes Somos?
                                    </li>
                                    <li>
                                        <a href="conocenos-objetivos.php" class="full"></a>
                                        Objetivos
                                    </li>
                                    <li>
                                        <a href="conocenos-nuestro-compromiso.php" class="full"></a>
                                        Nuestro Compromiso
                                    </li>
                                    <li>
                                        <a href="conocenos-nuestro-equipo.php" class="full"></a>
                                        Nuestro Equipo
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2>
                                    COMITé CONSULTIVO
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="texto">
                                            La Organización Mundial de Apoyo a la Educación, OMA es una asociación sin fines de lucro orientada a contribuir a la formación de profesionales íntegros y comprometidos con su país.  
                                        </div>
                                        <div class="lista_nuestro_equipo">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-3">
                                                    <article>
                                                        <div class="imagen">
                                                            <img src="images/comite-foto-1.jpg">
                                                        </div>
                                                        <div class="name">
                                                            ROQUE BENAVIDES
                                                        </div>
                                                        <div class="cargo">
                                                            Presidente Ejecutivo en Compañia de Minas Buenaventura S.A.A.
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="col-sm-6 col-md-3">
                                                    <article>
                                                        <div class="imagen">
                                                            <img src="images/comite-foto-2.jpg">
                                                        </div>
                                                        <div class="name">
                                                        EDUARDO RUBIO
                                                        </div>
                                                        <div class="cargo">
                                                        Asesor de Anglo American Perú S.A.
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="col-sm-6 col-md-3">
                                                    <article>
                                                        <div class="imagen">
                                                            <img src="images/comite-foto-3.jpg">
                                                        </div>
                                                        <div class="name">
                                                        MARIA DEL PILAR BENAVIDES ALFARO
                                                        </div>
                                                        <div class="cargo">
                                                        Presidenta y fundadora de OMA
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="col-sm-6 col-md-3">
                                                    <article>
                                                        <div class="imagen">
                                                            <img src="images/comite-foto-4.jpg">
                                                        </div>
                                                        <div class="name">
                                                        JAVIER VELARDE
                                                        </div>
                                                        <div class="cargo">
                                                        2nd Regional Vice President External Relations South America Newmont Mining Corporation
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="col-sm-6 col-md-3">
                                                    <article>
                                                        <div class="imagen">
                                                            <img src="images/comite-foto-5.jpg">
                                                        </div>
                                                        <div class="name">
                                                        MARY CLAUX
                                                        </div>
                                                        <div class="cargo">
                                                        Doctora en Ciencias Sociales
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="col-sm-6 col-md-3">
                                                    <article>
                                                        <div class="imagen">
                                                            <img src="images/comite-foto-6.jpg">
                                                        </div>
                                                        <div class="name">
                                                        NAHIL HIRSH
                                                        </div>
                                                        <div class="cargo">
                                                        Gerente de Políticas Públicas de Yanacocha
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="col-sm-6 col-md-3">
                                                    <article>
                                                        <div class="imagen">
                                                            <img src="images/comite-foto-7.jpg">
                                                        </div>
                                                        <div class="name">
                                                        ISABEL ARIAS
                                                        </div>
                                                        <div class="cargo">
                                                        Presidenta Ejecutiva de Minera San Ignacio de Morococha SIMSA
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="col-sm-6 col-md-3">
                                                    <article>
                                                        <div class="imagen">
                                                            <img src="images/comite-foto-8.jpg">
                                                        </div>
                                                        <div class="name">
                                                        YOLANDA LAUMER
                                                        </div>
                                                        <div class="cargo">
                                                        Directora de Desarrollo Empresarial en Apu Resources
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="col-sm-6 col-md-3">
                                                    <article>
                                                        <div class="imagen">
                                                            <img src="images/comite-foto-9.jpg">
                                                        </div>
                                                        <div class="name">
                                                        SUSANA WATSON
                                                        </div>
                                                        <div class="cargo">
                                                        Vicepresidenta y Fundadora de OMA
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="col-sm-6 col-md-3">
                                                    <article>
                                                        <div class="imagen">
                                                            <img src="images/comite-foto-10.jpg">
                                                        </div>
                                                        <div class="name">
                                                        ANA MARÍA PLENGE
                                                        </div>
                                                        <div class="cargo">
                                                        Expresidenta de Waaime Perú
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="texto">
                                            <div class="subtitulo">NUESTRA ORGANIZACIÓN</div>
                                            <p>La Organización Mundial de Apoyo a la Educación, OMA, es una organización sin fines de lucro orientada a contribuir a la formación de profesionales íntegros y comprometidos con su país.</p>                                            
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="texto">                                            
                                            <div class="subtitulo">NUESTRA MISIÓN</div>
                                            <p>Empoderar a los jóvenes como agentes de cambio capaces de enfrentar desafíos y buscar soluciones innovadoras que aporten al bienestar y desarrollo sostenible de sus regiones y del país.</p>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="texto">
                                            <div class="subtitulo">NUESTRA HISTORIA</div>
                                            <p>Conscientes de la importancia que tiene el desarrollo de las habilidades blandas y de cómo estas promueven el talento humano nace OMA, liderada por su fundadora Pilar Benavides.   </p>

                                            <p>Hoy se necesita que los profesionales posean, además de conocimientos técnicos y académicos, las capacidades para trabajar en armonía con sus colaboradores. El profesional debe ser ético, responsable, innovador, motivador y un buen comunicador. Estas habilidades no se adquieren de manera teórica, sino a través de talleres que incentiven la reflexión y el autoconocimiento, así como el trabajo en equipo. </p>

                                            <p>Si bien las universidades incluyen en sus mallas curriculares el aprendizaje teórico de estas competencias, no ofrecen las herramientas ni las estrategias adecuadas para reforzarlas. El propósito de OMA es compensar esta carencia al ofrecer talleres vivenciales gratuitos que involucran componentes emocionales y cognitivos, logrando que el aprendizaje sea sostenible y aplicable a distintos contextos de trabajo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>

                <?php include 'seccion_testimonios.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>