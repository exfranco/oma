<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
     <?php include 'metas.html';?>

</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_chico"  style="background-image: url(images/banner-contactenos.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            Contáctenos
                        </h1>
                    </div>
                </div>								
			</div>

            <section class="seccion_donaaqui">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="contactenos.php" class="activo">Contáctenos</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">   
                            <div class="col-12">
                                <h2>
                                    Contáctenos
                                </h2>
                                <div class="formulario">
                                <div class="message" style="display: none;">
                                    <h3 style="text-align: center;">
                                        Sus datos se han enviado con éxito!
                                    </h3>
                                </div>
                                <form id="frmContact" method="post" action="https://docs.google.com/forms/d/e/1FAIpQLScdwZ7LaQC9ct7tj4c6YDuOD6K6EZzIYyA1IwO5GkVddiyX6g/formResponse">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" name="entry.1935694469" placeholder="Asunto (*)" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" name="entry.1900325070" placeholder="Nombre (*)" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" name="entry.675738033" placeholder="Apellido (*)" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="number" name="entry.778842584" placeholder="Teléfono">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="email" name="entry.654466304" placeholder="E-mail (*)" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" name="entry.55568445" placeholder="Ciudad">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" name="entry.1379694622" placeholder="País">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="campo">
                                                <textarea name="entry.889289627" placeholder="Comentario (*)" required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="campo_check">

                                                <label class="cont">

                                                    Acepto los términos y condiciones

                                                    <input type="checkbox" id="acceptance" name="entry.813839146" value="Si">

                                                    <span class="checkmark"></span>                                    

                                                </label>

                                                
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6"></div>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn_enviar">
                                                Enviar
                                            </button>
                                        </div>

                                       
                                    </div>
                                </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>
 
    <script src="js/bootstrap-select.js"></script>
    <script src="js/forms.js?v=1.01"></script>
    <script>
        function createOptions(number) {
            var options = [], _options;
            for (var i = 0; i < number; i++) {
                var option = '<option value="' + i + '">Asunto ' + i + '</option>';
                options.push(option);
            }
            _options = options.join('');          
            $('#selectAsunto')[0].innerHTML = _options;
        }
        createOptions(6);
    </script>
</body>
</html>