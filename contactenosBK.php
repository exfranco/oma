<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
     <?php include 'metas.html';?>

</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_chico"  style="background-image: url(images/banner-contactenos.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            Contáctenos
                        </h1>
                    </div>
                </div>								
			</div>

            <section class="seccion_donaaqui">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="contactenos.php" class="activo">Contáctenos</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">   
                            <div class="col-12">
                                <h2>
                                    Contáctenos
                                </h2>
                                <div class="formulario">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" placeholder="Asunto">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" placeholder="Nombre">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" placeholder="Apellido">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" placeholder="Teléfono">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" placeholder="E-mail">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" placeholder="Ciudad">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="campo">
                                                <input type="text" placeholder="País">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="campo">
                                                <textarea name="" id="" placeholder="Comentario"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="campo_check">

                                                <label class="cont">

                                                    Acepto los términos y condiciones

                                                    <input type="checkbox" id="check1" name="" value="Si">

                                                    <span class="checkmark"></span>                                    

                                                </label>

                                                
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6"></div>
                                        <div class="col-md-6">
                                            <div class="btn_enviar">
                                                Enviar
                                            </div>
                                        </div>

                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>
 
    <script src="js/bootstrap-select.js"></script>     
    <script>
        function createOptions(number) {
            var options = [], _options;
            for (var i = 0; i < number; i++) {
                var option = '<option value="' + i + '">Asunto ' + i + '</option>';
                options.push(option);
            }
            _options = options.join('');          
            $('#selectAsunto')[0].innerHTML = _options;
        }
        createOptions(6);
    </script>
</body>
</html>