<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_chico"  style="background-image: url(images/banner-dona-aqui.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            Dona Aquí
                        </h1>
                    </div>
                </div>								
			</div>

            <section class="seccion_donaaqui">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="dona-aqui.php" class="activo">Dona Aquí</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_donaaqui_detalle">
                    <div class="container">
                        <div class="row">   
                            <div class="col-12">
                                <h2>
                                    Dona Aquí
                                </h2>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="titulo">
                                    Donación
                                </div>
                                <div class="texto">
                                    Tu colaboración es indispensable para continuar con nuestro trabajo. Si deseas hacer una donación voluntaria puedes hacerlo a través de una transferencia bancaria a las siguiente cuentas:
                                </div>
                                <div class="tit">
                                    Banco Interbank
                                </div>
                                <div class="sub">
                                    ORGANIZACIÓN MUNDIAL DE APOYO A LA EDUCACIÓN
                                </div>
                                <div class="texto">
                                    <p>
                                        Número de cuenta en dólares: 316-300113311-4 <br>
                                        CCI : 003-316-003001133114-74
                                    </p>
                                    <p>
                                        Número de cuenta en soles: 316-300113310-7<br>
                                        CCI : 003-316-003001133107-78
                                    </p>

                                    <p>RUC: 20600113748</p>
                                </div>

                                <div class="tit2">
                                    Beneficios:
                                </div>
                                <ul class="lista_check_orange">
                                    <li>Invitaciones al Congreso OMA</li>
                                    <li>Certificado de donación</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <div class="imagen">
                                    <img src="images/dona-aqui-img-1.jpg">
                                </div>
                                <div class="titulo">
                                    Donación por Taller
                                </div>
                                <div class="texto">
                                    Taller en Lima $ 2,286 <br>
                                    Taller en provincia $ 3,429
                                </div>
                                <div class="tit2">
                                    Beneficios:
                                </div>
                                <ul class="lista_check_orange">
                                    <li>Invitaciones al Congreso OMA</li>
                                    <li>Certificado de donación</li>
                                    <li>Invitaciones a eventos OMA</li>
                                </ul>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="imagen">
                                    <img src="images/dona-aqui-img-2.jpg">
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="titulo">
                                    Auspicio
                                </div>
                                <div class="sub">
                                    Gestor del aprendizaje: $ 5,000
                                </div>
                                <div class="tit2">
                                    Beneficios:
                                </div>
                                <ul class="lista_check_orange">
                                    <li>Invitaciones al Congreso OMA</li>
                                    <li>Certificado de donación</li>
                                    <li>Invitaciones a eventos OMA</li>
                                    <li>Auspiciador en todos los eventos</li>
                                    <li>Solicitar 3 talleres de Coaching</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="sub">
                                    Líder del Cambio OMA: $ 14,000
                                </div>
                                <div class="tit2">
                                    Beneficios:
                                </div>
                                <ul class="lista_check_orange">
                                    <li>Invitaciones al Congreso OMA</li>
                                    <li>Certificado de donación</li>
                                    <li>Invitaciones a eventos OMA</li>
                                    <li>Auspiciador en todos los eventos</li>
                                    <li>Solicitar 3 talleres de Coaching</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <div class="sub">
                                    Promotor de Líderes OMA: $ 9,000
                                </div>
                                <div class="tit2">
                                    Beneficios:
                                </div>
                                <ul class="lista_check_orange">
                                    <li>Invitaciones al Congreso OMA</li>
                                    <li>Certificado de donación</li>
                                    <li>Invitaciones a eventos OMA</li>
                                    <li>Auspiciador en todos los eventos</li>
                                    <li>Solicitar 2 taller de Coaching</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>