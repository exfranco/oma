<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body id="page-home"> 	
 	<div id="loader"></div>
 	<div class="wrapper wrapper_home"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner">
				<ul class="slider">
					<li style="background-image: url(images/banner-aprendizaje.jpg);">
						<div class="container container_left">
							<div class="box">
								<h1 class="txt_left">
									OMA: <br>
									aprendizaje que <br>
									transforma vidas
								</h1>
								<div class="ver_video">
									<a href="https://www.youtube.com/watch?v=Drp12oU21zU&feature=youtu.be" class="full fancybox_video"></a>
									<img src="images/ver-video.svg">
								</div>
							</div>
						</div>
					</li>
					<li style="background-image: url(images/banner.jpg);">
						<div class="container">
							<div class="box">
								<h1>
									Aprendizaje <br> que transforma Vidas
								</h1>
								<div class="subtitulo">
									El talento está formado por la suma de conocimientos, competencias <br> blandas, ética, compromiso y acción.
								</div>

								<div class="btn_dona">
									<a href="dona-aqui.php" class="full"></a>
									Dona Aquí
								</div>
							</div>
						</div>
					</li>
					<li style="background-image: url(images/banner-2.jpg);">
						<div class="container container_left">
							<div class="box _align_left">
								<h1>
								ACELERA TU ÉXITO <br> FORTALECE TUS HABILIDADES

								</h1>
								<div class="small_text">
									Te ayudaremos a alcanzar tus metas 					

								</div>
								<div class="subtitulo">
									Saber apreciar y valorar cada una de nuestras virtudes y aprender de cada <br> error es lo que nos lleva al logro de nuestras metas y proyectos.
								</div>

								<div class="btn_dona">
									<a href="dona-aqui.php" class="full"></a>
									Dona Aquí
								</div>
							</div>
						</div>
					</li>
					<li style="background-image: url(images/banner-3.jpg);">
						<div class="container">
							<div class="box _align_left">
								<div class="mapa">
									<img src="images/home-banner-mapa.png">
								</div>
								<h1>
								FORMACIÓN DE ÉXITO
								</h1>
								<div class="subtitulo">
									+ 20,000 estudiantes capacitados empleables en el sector minero y energético
								</div>

								<div class="btn_dona">
									<a href="dona-aqui.php" class="full"></a>
									Dona Aquí
								</div>

								<div class="numero_big">
									+20,000
								</div>
							</div>
						</div>
					</li>
				</ul>
				
			</div>

			<div class="seccion_organizacion">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="h2">
								<span>ORGANIZACIÓN MUNDIAL</span> DE APOYO A LA EDUCACIÓN
							</div>

							<div class="subtitulo">
								OMA es una asociación sin fines de lucro orientada a contribuir a la formación de profesionales íntegros y comprometidos con su país.
							</div>
						</div>
						<div class="col-md-3">
							<article>
								<a href="actividades-congreso-oma.php" class="full"></a>
								<div class="imagen">
									<img src="images/home-organizacion-1.jpg">
								</div>
								<div class="caja">
									<div class="tit">Congreso oma</div>
									<div class="texto">
										Evento bianual que convoca estudiantes de alto rendimiento académico del país y latinoamérica para asistir a talleres de coaching, mentoring y foros educativos, organizados en el marco de Expomina
									</div>
									<div class="leer">
										Leer más -->
									</div>
								</div>
							</article>
						</div>
						<div class="col-md-3">
							<article>
								<a href="actividades-programa-mujeres-roca.php" class="full"></a>
								<div class="imagen">
									<img src="images/home-organizacion-2.jpg">
								</div>
								<div class="caja">
									<div class="tit">Programa <br>mujeres Roca</div>
									<div class="texto">
										Programa en alianza con la Cámara de Comercio de Canadá, que busca contribuir a la diversidad de género en posiciones gerenciales dentro del sector minero en el Perú.
									</div>
									<div class="leer">
										Leer más -->
									</div>
								</div>
							</article>
						</div>
						<div class="col-md-3">
							<article>
								<a href="actividades-programa-de-coaching.php" class="full"></a>
								<div class="imagen">
									<img src="images/home-organizacion-3.jpg">
								</div>
								<div class="caja">
									<div class="tit">Programa <br>de Coaching</div>
									<div class="texto">
									Liderazgo en valores para la comunidad. Desarrollo de competencias para la inserción laboral Desarrollo de competencias para el docente universitario Inducción a la vida universitaria Desarrollo de competencias para el liderazgo, la comunicación efectiva y trabajo en equipo
									</div>
									<div class="leer">
										Leer más -->
									</div>
								</div>
							</article>
						</div>
						<div class="col-md-3">
							<article>
								<a href="actividades-asociacion-oma.php" class="full"></a>
								<div class="imagen">
									<img src="images/home-organizacion-4.jpg">
								</div>
								<div class="caja">
									<div class="tit">Asociación OMA</div>
									<div class="texto">
										Liderazgo en valores para la comunidad. Desarrollo de competencias para la inserción laboral Desarrollo de competencias para el docente universitario Inducción a la vida universitaria Desarrollo de competencias para el liderazgo, la comunicación efectiva y trabajo en equipo
									</div>
									<div class="leer">
										Leer más -->
									</div>
								</div>
							</article>
						</div>
					</div>
				</div>
			</div>

			<?php include 'seccion_gracias_auspiciadores.html';?>
			<?php include 'seccion_alianzas.html';?>
		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>