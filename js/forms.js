window.onload = function() {
	init();
};

function init(){
	const frmContact = document.getElementById('frmContact');
	if(frmContact){
		frmContact.addEventListener('submit', submit_form);
	}
}

function post_form(url, params, callback){
	fetch(url, {
		method: 'POST',
		body: params,
		mode: "no-cors",
		cache: "no-cache",
		credentials: "same-origin",
		headers: {
			"Content-Type": "application/json",
		},
	})
	.then(response => callback(response))
	.catch(error => {
		console.error(error);
		alert(error);
	});
}

submit_form = async (e)=>{
	e.preventDefault();

	const acceptance = document.querySelector('#acceptance');

	if(!acceptance.checked){
		alert('Por favor, debe aceptar los términos y condiciones.');
		return false;
	}

	const form = e.target;
	const url = form.getAttribute('action');
	const params = new FormData(form);

	const message = document.querySelector('.message');
	const submit = document.querySelector('button[type=submit]');

	if(submit.innerHTML=='Enviando...') return false;

	submit.innerHTML='Enviando...';

	post_form(url, params, function(resp){
		console.log(resp);
		form.style.display = 'none';
		message.style.display = 'block';
		submit.innerHTML='Enviar';
		window.scrollTo(0, 0);
	});

  return false;

}
