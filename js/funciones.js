$( document ).ready(function() {
    menu_activo();
    carrousel();
    fancys();
    menu_mobile();
    slider();
    acordiones();
    menuLateral();
    scrollTop();
    precarga_home();
  
  
    if($(".wrapper_home").length > 0){  
      $(".n2-style-64790101d46086613c81de0c0ce3eb20-heading.n2-ow").addClass("fancybox_video");

      $(".n2-style-64790101d46086613c81de0c0ce3eb20-heading.n2-ow").click(function(){
        
        console.log("banner video");
        $.fancybox({
            'padding'   : 0,
            'autoScale'   : false,
            'transitionIn'  : 'none',
            'transitionOut' : 'none',
            'title'     : this.title,
            'width'     : 800,
            'height'    : 480,
            'href'      : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type'      : 'swf',
            'swf'       : {
                'wmode'       : 'transparent',
                'allowfullscreen' : 'true',
            }
        });
        return false;
      })

        
    }  

});


$( window ).resize(function() {

});



function precarga_home(){
  var imageload = {
    1 : 'images/banner.jpg',
  };
  var loader = new PxLoader();
  for( pos in imageload ){
    var pxImage = new PxLoaderImage( imageload[pos] );
    loader.add(pxImage);
  }
  loader.addProgressListener(function(e){
    var percentage = Math.ceil( ( e.completedCount / e.totalCount ) * 100 );
  });
  loader.addCompletionListener(function(){
    $('#loader').fadeOut("slow");    
    var timer=false
    timer = setTimeout(function(){
    
    }, 10)
  });
  loader.start();
}

function menu_activo(){
  if($('.seccion_conocenos').length > 0){
      $("li.menu_conocenos").addClass("activo");
  }
  if($('.seccion_quehacemos').length > 0){
      $("li.menu_quehacemos").addClass("activo");
  }
  if($('.seccion_coaching').length > 0){
      $("li.menu_coaching").addClass("activo");
  }
  if($('.seccion_mentorias').length > 0){
      $("li.menu_mentorias").addClass("activo");
  }
  if($('.seccion_becas').length > 0){
    $("li.menu_becas").addClass("activo");
  }
  if($('.seccion_bolsa').length > 0){
      $("li.menu_bolsa").addClass("activo");
  }
  if($('.seccion_blog').length > 0){
    $("li.menu_blog").addClass("activo");
  }
}


function carrousel(){
    var owl = $('.seccion_alianzas .lista_logos, .seccion_gracias_auspiciadores .lista_logos');
    owl.owlCarousel({
        margin: 20,
        loop: true,
        nav: true,
        autoplay: true,
        autoplayTimeout: 3000,
        responsive: {
            0: {
            items: 1
            },
            600: {
            items: 2
            },
            800: {
            items: 3
            },
            1000: {
            items: 4
            },
            1300: {
            items: 5
            }
        }
    })
}

function fancys(){
    $('.fancybox').fancybox();

    if(location.hash){
        $(location.hash).click();
    }

    $(".fancybox_video").click(function() {
        $.fancybox({
            'padding'   : 0,
            'autoScale'   : false,
            'transitionIn'  : 'none',
            'transitionOut' : 'none',
            'title'     : this.title,
            'width'     : 800,
            'height'    : 480,
            'href'      : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type'      : 'swf',
            'swf'       : {
                'wmode'       : 'transparent',
                'allowfullscreen' : 'true',
            }
        });
        return false;
    }); 
}

function menu_mobile(){
    var timer = false;  
    timer = setTimeout(function(){    
  
      $(".menu_mobile .sanguche").on({
        click: function(e){         
  
          $(".wrapper").addClass("wrap_mobile");
          $("body").addClass("nonescroll");
          $("header").addClass("mobile");
          $(".menu_mobile").addClass("activado");
          //$(".sombra").fadeIn();
  
          var alturaWindow = $(window).height();
          $("header .contenedor_header").css({"height": alturaWindow});
          $("ul.menu li ul.sub_menu").hide();
          $("ul.menu li ul.sub_menu li").removeClass('abierto');
          $("ul.menu li").removeClass('abierto');
          $("ul.menu li ul.ultra_menu").hide('slow');
  
          $(".menu_mobile .sanguche").hide();
          $(".menu_mobile .mobile_tools").fadeIn();
        }
      })    
  
      $(".menu_mobile .mobile_tools .btn_close").on({
        click: function(e){        
          $(".wrapper").removeClass("wrap_mobile");
          $("body").removeClass("nonescroll");
          //$(".sombra").fadeOut();
          $("ul.menu li ul.sub_menu").hide();
          $("ul.menu li ul.sub_menu li").removeClass('abierto');
          $("ul.menu li").removeClass('abierto');
          $("ul.menu li ul.ultra_menu").hide('slow');
          $("header").removeClass("mobile");
          $(".menu_mobile").removeClass("activado"); 
          $(".menu_mobile .sanguche").fadeIn();
          $(".menu_mobile .mobile_tools").hide(); 
        }
      })  
  
    }, 200);
  
  
  $("ul.menu li .menu_padre").on({
    click: function(){
      if($(this).parent().hasClass("abierto")){
        $(this).parent().find("ul.sub_menu").slideUp();
        $(this).parent().find("ul.sub_menu li").removeClass('abierto');
        $(this).parent().removeClass('abierto');
        $(this).parent().find("ul.ultra_menu").slideUp('slow');
      }
      else{
        $(this).parent().find("ul.sub_menu").slideDown('slow');
        $(this).parent().toggleClass( "abierto" );
        $(this).parent().siblings().find("ul.sub_menu").slideUp('slow');
        $(this).parent().siblings().find("ul.ultra_menu").slideUp('slow');
        $(this).parent().siblings().removeClass('abierto');
        $(this).parent().siblings().find("ul.sub_menu li").removeClass('abierto');
      }
    }
  })
  
  $("ul.menu li ul.sub_menu li .hijo").on({
    click: function(){
      if($(this).parent().hasClass("abierto")){
        $(this).parent().find("ul.ultra_menu").slideUp();
        $(this).parent().removeClass('abierto');
      }
      else{
        $(this).parent().find("ul.ultra_menu").slideDown('slow');
        $(this).parent().toggleClass( "abierto" );
        $(this).parent().siblings().find("ul.ultra_menu").slideUp('slow');
        $(this).parent().siblings().removeClass('abierto');
      }      
    }
  })
  
  $("ul.menu li ul.sub_menu li ul.ultra_menu li .nieto").on({
    click: function(){
      if($(this).parent().hasClass("abierto")){
        $(this).parent().find("ul.menu_cuarto").slideUp();
        $(this).parent().removeClass('abierto');
      }
      else{
        $(this).parent().find("ul.menu_cuarto").slideDown('slow');
        $(this).parent().toggleClass( "abierto" );
        $(this).parent().siblings().find("ul.menu_cuarto").slideUp('slow');
        $(this).parent().siblings().removeClass('abierto');
      }      
    }
  })

    $("ul.menu li ul.sub_menu").parent().addClass("menu_plus");
    
    $("ul.menu li").hover(function() {
        $(this).siblings("li").removeClass("activo");
    }, function() {
        $(this).siblings("li.on").addClass("activo");
    });
}



function slider(){  
  $('.slider').bxSlider({
      mode: 'fade',
      pager: false,
      speed: 300,
      auto: true,
      pause: 7000,
      controls: true,
      touchEnabled: false,
      responsive: true,    
  });
}


function acordiones(){

  /*$("ul.lista_acordion li:eq(0)").addClass("activo");
  $("ul.lista_acordion li:eq(0) .desplegable").css("display" , "block");*/


  $("ul.lista_acordion li .titulo").on({
      click: function(){
          if ($(window).width() < 769){
              if($(this).parent().hasClass("activo")){
                  $(this).parent().siblings().find(".desplegable").slideUp();
                  $(this).parent().find(".desplegable").slideUp();
                  $(this).parent().removeClass("activo");
              }else{
                  $(this).parent().siblings().removeClass("activo");
                  $(this).parent().siblings().find(".desplegable").slideUp();
                  $(this).parent().find(".desplegable").slideDown();
                  $(this).parent().addClass("activo");
              }  
          } else{
              if($(this).parent().hasClass("activo")){
                  $(this).parent().siblings().find(".desplegable").hide();
                  $(this).parent().find(".desplegable").hide();
                  $(this).parent().removeClass("activo");
              }else{
                  $(this).parent().siblings().removeClass("activo");
                  $(this).parent().siblings().find(".desplegable").hide();
                  $(this).parent().find(".desplegable").fadeIn();
                  $(this).parent().addClass("activo");
              }  
          }         
      }
  })
}


function layerGallery(){
    var layer = document.getElementById("layerGallery");
    layer.style.display = "block"; 

}

function closeGallery(){
    var layer = document.getElementById("layerGallery");
    layer.style.display = "none"; 
}

function menuLateral(){
  $(".btn_menu_lateral").click(function(){
    if($(".menu_lateral").hasClass("activo")){
      $(".btn_menu_lateral").removeClass("activo");
      $(".menu_lateral").removeClass("activo");
      $(".menu_lateral").slideUp();
    }else{
      $(".btn_menu_lateral").addClass("activo");
      $(".menu_lateral").addClass("activo");
      $(".menu_lateral").slideDown();
    }
  })
}

function scrollTop(){
  $el = $('header');
  $el.toggleClass('headerEstatico', $(this).scrollTop() > 0);
 
  $(window).scroll( function() {
      $el = $('header');
      $el.toggleClass('headerEstatico', $(this).scrollTop() > 0);   
  });   
}

function layerGalleryFotoStandar(){
    
  $("article.eventos").click(function(){
      /*var layer = document.getElementById("layerGallery");
      layer.style.display = "block"; 
      var atributoImagen = $(this).attr("img");
      $(".layer_galeria .box .imagen").append('<img src="images/'+atributoImagen+'">');*/

      var imagenGrandeWidth = $(this).find(".imagen").find("img").width();
      var imagenGrandeHeight = $(this).find(".imagen").find("img").height();
      console.log("este es el ancho de la imagen " + imagenGrandeWidth);
      console.log("este es el alto de la imagen " + imagenGrandeHeight);
      var winHeight = $(window).height();
      var fotoHeight = winHeight - 30;

      if ($(window).width() > 768){
          if(imagenGrandeWidth <= imagenGrandeHeight){
              $(".galeria_standar .caja").addClass("box_height");
              $(".galeria_standar .caja.box_height .c .foto img").css({"height":fotoHeight});
          }else{
              $(".galeria_standar .caja").addClass("box_width");            
          }
      }else{
          if(imagenGrandeWidth <= imagenGrandeHeight){
              $(".galeria_standar .caja").addClass("box_height");
              $(".galeria_standar .caja.box_height .c .foto img").css({"max-height":fotoHeight});
          }
      }   
  })   
}


function layerLogin(){
  var layer = document.getElementById("layerLogin");
  layer.style.display = "block"; 

}

function closeLayerLogin(){
  var layer = document.getElementById("layerLogin");
  layer.style.display = "none"; 
}