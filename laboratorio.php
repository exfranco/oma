<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-laboratorio.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            <span>ASOCIACIÓN OMA</span> <br>                            
                            LABORATORIO DE COACHING <br>
                            “INNOVACIÓN Y GESTIÓN DE LA <br>
                            TRANSFORMACIÓN EN LA ERA <br>                            
                            DIGITAL”.
                        </h1>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="laboratorio.php" class="activo">Laboratorio</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_laboratorio">
                    <div class="container">
                        <div class="row">
                           
                            <div class="col-12">
                                <h2>
                                    Laboratorio
                                </h2>                                
                            </div>
                            <div class="col-md-12">
                                <div class="texto">
                                    Espacios de aprendizaje y experimentación grupal, en los que cada participante lleva a cabo una evaluación a 360°, aprendiendo estrategias y técnicas precisas de desarrollo humano y competencias blandas, finalmente, evalúa el impacto de la metodología aplicada en tiempo real.
                                    Cada laboratorio es diseñado de acuerdo con las necesidades específicas de la entidad que lo solicita. La duracion es de 16 horas en forma presencial y 10 horas de manera virtual. 
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="subtitulo">
                                    LABORATORIO DE COACHING “INNOVACIÓN Y GESTIÓN DE LA <br>TRANSFORMACIÓN EN LA ERA DIGITAL”. <br><br>
                                </div>
                                <div class="texto texto16">
                                    <span class="mayor">Objetivo:</span> <br><br>
                                    Desarrollar estrategias e implementar herramientas para la innovación y gestión de la transformación, acordes con las necesidades provocadas por la tecnología digital. favoreciendo el liderazgo y desarrollo humano en un contexto híbrido, de comunicación, trabajo y aprendizaje en las modalidades virtual y presencial. 
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="directora">
                                    <img src="images/laboratorio-claudia.jpg">
                                    <div class="name">
                                        Claudia Beltrán
                                    </div>
                                    <div class="cargo">
                                        Directora del programa de coaching y directora del programa Mujeres Roca en OMA
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="cuadro_gris_fecha">
                                    <div class="texto texto16">
                                        <span class="mayor">Fecha</span><br>
                                        lunes 25 y martes 26 de abril
                                        <br><br>
                                        <span class="mayor">Hora:</span> <br>
                                        8am a 12m <br>
                                        de 2 a 6pm
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="galeria">
                                    <div class="titulo">
                                        Galeria de Fotos
                                    </div>
                                    <div class="row">
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/laboratorio-img-1.jpg"></div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/laboratorio-img-2.jpg"></div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/laboratorio-img-3.jpg"></div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/laboratorio-img-4.jpg"></div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/laboratorio-img-5.jpg"></div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="foto"><img src="images/laboratorio-img-6.jpg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>