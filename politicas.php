<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
     <?php include 'metas.html';?>

</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_chico"  style="background-image: url(images/banner-politicas.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            Póliticas de Privacidad
                        </h1>
                    </div>
                </div>								
			</div>

            <section class="seccion_donaaqui">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="politicas.php" class="activo">Póliticas de Privacidad</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_politicas">
                    <div class="container">
                        <div class="row">   
                            <div class="col-12">
                                <h2>
                                    POLÍTICA DE PRIVACIDAD DE OMA
                                </h2>
                                
                            </div>
                            
                        </div>

                        <div class="texto">

                            <p>
                                Este sitio de internet (website) pertenece a Organización Mundial de Apoyo a la Educación (OMA). El uso de este sitio por parte de cada usuario: (i) está sometido a los términos y condiciones que se detallan más adelante (los “Términos y Condiciones); y (ii) constituye aceptación de estos Términos y Condiciones.
                                OMA podrá revisar, modificar y actualizar estos Términos y Condiciones en cualquier momento, a su total discreción.
                                Para su comodidad, se ha dividido los Términos y Condiciones, en tres grupos. Estos son:
                            </p>

                            <p>
                                <span class="orange">Política de Privacidad (uso de la información por los usuarios)</span> <br>
                                Propiedad intelectual y derechos de autor (prohibición de utilizar los contenidos, diseños y demás elementos de este sitio o página web).
                                Exclusión de responsabilidad (por daños a los equipos o a los programas causados por cualquier clase de virus o por cualquier otra circunstancia relacionada directa o indirectamente con el acceso a la página web de OMA).
                            </p>

                            <ol class="lista_numeros">
                                <li>
                                    <div class="texto">
                                        <span class="orange">POLÍTICA DE PRIVACIDAD</span>
                                        <p>1.1. Queda expresamente establecido que por medio del cliqueo que el usuario realice en la web de OMA se otorga expresa autorización para la recopilación y tratamiento de sus Datos Personales que sean facilitados o que se faciliten a través de su ingreso al portal web o por cualquier medio sean recolectados y tratados por OMA.</p>

                                        <p>En ese sentido, el usuario le brinda a OMA su consentimiento libre, previo, expreso e informado para que sus Datos Personales sean tratados por OMA, es decir, que puedan ser: recopilados, registrados, organizados, almacenados, conservados, elaborados, modificados, bloqueados, suprimidos, extraídos, consultados, utilizados, transferidos o procesados de cualquier otra forma prevista por Ley.</p>

                                        <p>1.2. OMA utilizará la información personal que los usuarios le suministren voluntaria y conscientemente, para la conformación de su base de datos sobre los visitantes a su página web, para el conocimiento y atención de sus clientes, y para sus actividades de mercadeo en general.</p>

                                        <p>OMA utilizará la información personal que le suministren los visitantes de la página web para mejorar sus esfuerzos publicitarios, comerciales, de mercadeo y promocionales; para usos y análisis estadísticos sobre el uso de la página web; y para mejorar las características y contenidos de la misma. También utilizará la información personal para enviar mensajes y contenidos que puedan ser de interés específico de los visitantes, de acuerdo con nuestro entendimiento de tal información personal suministrada por el visitante. Esta información comprenderá nuevos productos y servicios, características de éstos y aquellos, encuestas, oportunidades para efectuar compras en línea, promociones y concursos y vínculos con otras páginas web. Esta información podrá ser enviada por OMA y en consecuencia referirse a sus productos, o por otras compañías, seleccionadas cuidadosamente por OMA, que enviarán información acerca de sus propios productos y servicios, a menos que el visitante que haya entregado información personal manifieste su intención expresamente de que su información personal no sea compartida con otras compañías.</p>

                                        <p>1.3. Los usuarios que deseen dejar de recibir información y mensajes de OMA a través de su correo electrónico, tendrán la opción de manifestarlo así a esta compañía, la cual procederá a excluir a tal usuario de su lista de contactos de su correo electrónico, y se abstendrá en lo sucesivo de remitir información por esta vía.</p>

                                        <p>1.4. Los datos personales de los usuarios serán incorporados al banco de base de datos personales de clientes y posibles clientes de titularidad de OMA. Sus datos serán utilizados en la gestión administrativa y comercial de la compañía. Los datos personales se mantendrán en el banco de base de datos mientras se consideren útiles para los fines antes indicados.</p>

                                        <p>1.5. OMA procura, con los mejores medios de seguridad organizativos, legales y técnicos a su alcance y legalmente requeridos, que los terceros que prestan servicios a OMA relacionados con la página web, tales como el diseño, administración y actualización del sitio en internet, tengan acceso a la información que suministran los usuarios y la traten con la misma reserva y confidencialidad con que la utiliza OMA, pero no es responsable de ningún uso indebido o no autorizado que tales terceros llegaren a darle a la susodicha información.</p>

                                        <p>1.6. OMA actualizará tanto la información personal como la información adicional de los usuarios de su página web que éstos hayan proporcionado y deseen cambiar.</p>

                                        <p>1.7. Al usuario puede ejercer los derechos ARCO (acceso, rectificación, oposición y cancelación de los datos personales), los cuales podrá hacer efectivos mediante la presentación de una solicitud ante nuestras oficinas o en nuestra web.</p>

                                        <p>1.8. OMA podrá compartir información general sobre los visitantes, como por ejemplo la proporción de hombres y mujeres, sus rangos de edades, y sus sistemas de acceso a internet, con anunciantes, asociados, patrocinadores y cualesquiera otros terceros, con el propósito de mejorar el contenido y los anuncios de nuestro sitio.</p>

                                        <p>1.9. OMA podrá utilizar la información personal de los visitantes a su página web, para permitir a otras compañías que se pongan en contacto con OMA en lo referente a productos y servicios que pueden serle de interés, a menos que el usuario indique que no desea que esta información sea compartida.</p>
                                    </div>
                                </li>

                                <li>
                                    <div class="texto">
                                        <span class="orange">PROPIEDAD INTELECTUAL Y DERECHOS</span>

                                        <p>2.1. OMA mantiene este sitio web para brindar información acerca de sus servicios y como medio de interrelación con sus colaboradores. Las marcas, nombres comerciales, reseñas, gráficos, fotografías, dibujos, diseños y cualquiera otra propiedad que aparezca en este sitio web, así como los contenidos, están protegidas legalmente a favor de esta compañía de conformidad con las disposiciones legales sobre propiedad industrial y sobre derechos de autor. En consecuencia, no podrán ser utilizadas, modificadas, copiadas, reproducidas, transmitidas o distribuidas de ninguna manera, salvo previa autorización escrita y expresa de OMA.</p>

                                        <p>2.2. Los contenidos protegidos de conformidad con el párrafo anterior, incluyen textos, imágenes, ilustraciones, dibujos, diseños, software, música, sonido, fotografías y videos, y cualesquiera otros medios o formas de difusión de contenidos, información o conocimientos.</p>

                                        <p>2.3. Las ideas, opiniones, sugerencias y comentarios que sean enviadas en forma espontánea y sin previa solicitud por los usuarios de OMA, relativas a servicios o de cualquier otro tipo, serán usadas libremente por OMA si así lo estima conveniente. Tales comunicaciones no son confidenciales y se presume que no están protegidas por ninguna regulación atinente a derechos de autor o a propiedad intelectual, y por ende, el usuario remitente de tal información, no podrá reclamar indemnización o participación alguna en virtud del legítimo uso comercial que OMA de a la misma. Si la información estuviera protegida por algún tipo de regulación sobre derechos de autor o propiedad intelectual, su comunicación espontánea a OMA se entenderá como una renuncia total e irrevocable a los derechos y privilegios morales y patrimoniales que tales regulaciones otorgan, y la transferencia de los mismos a OMA. En todo caso, sea que la información suministrada esté protegida o no, la misma se convertirá y permanecerá de propiedad de OMA, lo cual significa que esta compañía no tratará esta información como confidencial; que el usuario remitente de la información no podrá reclamar judicial ni extrajudicialmente por el uso de esa información; y que tal usuario no tendrá derecho a pago alguno en virtud del uso de la información así suministrada.</p>


                                    </div>
                                </li>

                                <li>
                                    <div class="texto">
                                        <span class="orange">EXCLUSIONES DE RESPONSABILIDAD</span>
                                        <p>3.1. El material y los componentes técnicos en este sitio web pueden incluir errores y fallas técnicas, y por ende la visita que haga cada usuario será bajo su entero riesgo. Ni OMA ni los terceros involucrados en el diseño, administración y actualización del sitio, serán responsables de los daños que eventualmente llegaren a sufrir los usuarios directa o indirectamente, o como consecuencia del acceso o el uso del sitio web. OMA no garantiza que el acceso a su sitio web sea permanente o libre de errores, o se encuentre libre de virus o de otros componentes dañinos. En consecuencia, OMA no será responsable de los daños sufridos por el uso de su sitio web, causados, entre otras circunstancias, por el uso o falta de acceso a dicho sitio web; el uso o falta de acceso a un sitio web vinculado (“links”); fallas en el desempeño del sitio web; errores en el mismo; omisiones; interrupciones; demoras en la operación o en la transmisión; fallas en la línea, y similares.</p>

                                        <p>3.2. OMA selecciona cuidadosamente los vínculos (“links”) con otros sitios de Internet, a los que se tiene acceso a través de su sitio web, pero no se hace responsable del contenido ni de los aspectos técnicos de ellos, máxime si se tiene en cuenta que tales contenidos varían de tiempo en tiempo. En consecuencia, su acceso a tales sitios a través del sitio web de OMA se hará bajo el entero riesgo y responsabilidad del usuario.</p>

                                        <p>3.3. OMA no garantiza, ni expresa ni implícitamente, los contenidos incorporados en este sitio web, ni los elementos funcionales del mismo. Especialmente, no garantiza que:</p>

                                        <ul class="lista_check_orange">
                                            <li>Los elementos funcionales de este sitio web no tengan interrupciones o carezcan de errores.</li>
                                            <li>Los errores sean corregidos.</li>
                                            <li>El servidor de este sitio web, o el de los vinculados (“links”) por medio de los cuales hacen disponible este sitio web o aquellos, estén libres de virus o de otros componentes dañinos.</li>
                                            <li>El resultado de su visita y el de las actividades desarrolladas como consecuencia de su visita, sean completamente satisfactorios.</li>
                                        </ul>



                                    </div>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>
 
    <script src="js/bootstrap-select.js"></script>
    <script src="js/forms.js?v=1.01"></script>
    <script>
        function createOptions(number) {
            var options = [], _options;
            for (var i = 0; i < number; i++) {
                var option = '<option value="' + i + '">Asunto ' + i + '</option>';
                options.push(option);
            }
            _options = options.join('');          
            $('#selectAsunto')[0].innerHTML = _options;
        }
        createOptions(6);
    </script>
</body>
</html>