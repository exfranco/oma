<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-que-hacemos.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            EDUCACIÓN ESPERANZA FUTURO
                        </h1>
                        <div class="subtitulo">
                            El talento está formado por la suma de conocimientos, <br>
                             competencias blandas, ética, compromiso y acción.
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-actividades.php" class="activo">Actividades</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_actividades">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3 order-md-1"><div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral">
                                    <li class="activo">
                                        <a href="que-hacemos-actividades.php" class="full"></a>
                                        Actividades
                                    </li>
                                    <li>
                                        <a href="que-hacemos-programacion.php" class="full"></a>
                                        Programa 2022
                                    </li>
                                    <li>
                                        <a href="que-hacemos-testimonios.php" class="full"></a>
                                        Testimonios
                                    </li>
                                    <li>
                                        <a href="que-hacemos-logros.php" class="full"></a>
                                        Logros
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2>
                                    Actividades
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="lista_actividades">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <a href="actividades-programa-mujeres-roca.php" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/que-hacemos-actividades-1.jpg">
                                                </div>
                                                <div class="b">
                                                    <div class="titulo">
                                                        Programa Mujeres Roca
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <a href="actividades-programa-de-coaching.php" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/que-hacemos-actividades-2.jpg">
                                                </div>
                                                <div class="b">
                                                    <div class="titulo">
                                                        Programa de Coaching
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <a href="actividades-asociacion-oma.php" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/que-hacemos-actividades-3.jpg">
                                                </div>
                                                <div class="b">
                                                    <div class="titulo">
                                                        Asociación OMA
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                   
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <a href="actividades-mentoring.php" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/que-hacemos-actividades-4.jpg">
                                                </div>
                                                <div class="b">
                                                    <div class="titulo">
                                                        Mentoring
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <a href="actividades-curso-de-quechua.php" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/que-hacemos-actividades-5.jpg">
                                                </div>
                                                <div class="b">
                                                    <div class="titulo">
                                                        Curso de Quechua
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <a href="actividades-congreso-oma.php" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/que-hacemos-actividades-6.jpg">
                                                </div>
                                                <div class="b">
                                                    <div class="titulo">
                                                        Congreso OMA
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <a href="actividades-ponencias-para-comunidad-oma.php" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/que-hacemos-actividades-7.jpg">
                                                </div>
                                                <div class="b">
                                                    <div class="titulo">
                                                        Ponencias para Comunidad Oma
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                   
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <a href="actividades-voluntariado-en-proyectos-de-desarrollo.php" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/que-hacemos-actividades-8.jpg">
                                                </div>
                                                <div class="b">
                                                    <div class="titulo">
                                                        VOLUNTARIADO EN 
                                                        PROYECTOS DE DESARROLLO 
                                                        SOSTENIBLE Y 
                                                        RESPONSABILIDAD SOCIAL
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    
                                        <div class="col-sm-6 col-md-4">
                                            <article>
                                                <a href="actividades-voluntariado-de-la-comunidad-oma.php" class="full"></a>
                                                <div class="imagen">
                                                    <img src="images/que-hacemos-actividades-9.jpg">
                                                </div>
                                                <div class="b">
                                                    <div class="titulo">
                                                        VOLUNTARIADO 
                                                        DE LA COMUNIDAD OMA
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php include 'seccion_testimonios.html';?>

                <?php include 'seccion-nuestro-blog.html';?>
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>