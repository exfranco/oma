<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-logros.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            Proyecto Servir
                        </h1>
                        <div class="subtitulo">
                            6 Ugeles de Cajamarca + OMA  <br>
                            + 115 voluntarios + 460 estudiantes <br>
                            En coordinación con MINEDU
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-testimonios.php" class="activo">Logros</a></li>
                        </ul>
                    </div>                   											
                </div>

               
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2>
                            Logros
                            </h2>
                        </div>
                        <div class="col-md-3 order-md-1">
                            <div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                            <ul class="menu_lateral">
                                <li>
                                    <a href="que-hacemos-actividades.php" class="full"></a>
                                    Actividades
                                </li>
                                <li>
                                    <a href="que-hacemos-programacion.php" class="full"></a>
                                    Programa 2022
                                </li>
                                <li>
                                    <a href="que-hacemos-testimonios.php" class="full"></a>
                                    Testimonios
                                </li>
                                <li class="activo">
                                    <a href="que-hacemos-logros.php" class="full"></a>
                                    Logros
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9 order-md-12">
                            <div class="texto">
                                <p>Capacitar, apoyar y coordinar con los voluntarios y las Ugeles el acompañamiento a los escolares de primaria y secundaria de las zonas más necesitadas del país, a fin de contribuir a su nivelación y regreso a clases presenciales.</p>

                                <p><span>47 Charlas virturtuales: </span> Habilidades blandas, Experiencias de vida, Tecnología, Responsabilidad social, Emprendedurismo, Innovación.</p>

                                <p><span>23 talleres virtuales y 2 presenciales: </span> universidades nacionales y organizaciones aliadas en 17 regiones del país.</p>

                                <p><span>Nuevos Proyectos de Responsabilidad Social en las escuelas y comunidades.</span></p>

                                <p><span>KURKU WIND: </span>Proyecto de generación de energía para las escuelas y lugares alejados de nuestro país auspiciado por OMA.</p>

                                <p><span>Curso de quechua: </span>Objetivo - cubrir las necesidades básicas de comunicación para que, desde una óptica cultural e histórica, el participante pueda acercarse a las costumbres y tradiciones de las regiones quechua hablantes.</p>

                                <p><span>15 becas de Data Science:</span> Objetivo - Capacitar en herramientas analíticas enfocadas en mejorar el perfil profesional. Promover la formación de talentos en igualdad de oportunidades.</p>

                                <p><span>6 meses, 56 horas totales de un taller de acompañamiento y soporte profesional.</span> Objetivos principales: Promover y motivar un proceso autoreflexivo que incentive el compromiso personal por incrementar las habilidades blandas y personales. Afianzar la comunicación asertiva en el grupo y con ello la empatía, escucha activa y sentido de comunidad impactando en las habilidades de contacto.</p>

                                <p><span>12 becas de la Escuela de Mentoring de España: </span> Sesiones de mentoring con mentores internacionales de la Escuela de Mentoring de España</p>
                            </div>

                            <div class="lista_logros_fotografias">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="video">
                                            <div class="contenedor_video">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/sUrwvY2K-A4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <img src="images/logros-img-1.jpg">

                                        <img src="images/logros-img-4.jpg">
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <img src="images/logros-img-2.jpg">

                                        <img src="images/logros-img-5.jpg">
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <img src="images/logros-img-3.jpg">

                                        <img src="images/logros-img-6.jpg">
                                    </div>
                                </div>
                            </div>
                        

                        

                        </div>
                        
                    </div>
                </div>

                <?php include 'seccion-nuestro-blog.html';?>
                

                
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>