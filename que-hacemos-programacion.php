<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner"  style="background-image: url(images/banner-ayudanos.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            AYÚDANOS A AYUDAR
                        </h1>
                        <div class="subtitulo">
                            OMA reconoce que la innovación es un eje central <br>
                            para el fortalecimiento económico y social.
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-programacion.php" class="activo">Programa 2022</a></li>
                        </ul>
                    </div>                   											
                </div>

                <div class="seccion_programacion_2022">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-3 order-md-1"><div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                                <ul class="menu_lateral">
                                    <li>
                                        <a href="que-hacemos-actividades.php" class="full"></a>
                                        Actividades
                                    </li>
                                    <li class="activo">
                                        <a href="que-hacemos-programacion.php" class="full"></a>
                                        Programa 2022
                                    </li>
                                    <li>
                                        <a href="que-hacemos-testimonios.php" class="full"></a>
                                        Testimonios
                                    </li>
                                    <li>
                                        <a href="que-hacemos-logros.php" class="full"></a>
                                        Logros
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <h2>
                                Programa 2022
                                </h2>
                            </div>
                            <div class="col-md-9 order-md-12">
                                <div class="texto">
                                    <p><span>Convenios con Minedu, Hub de Innovación e ISE.</span></p>
                                    <p><span>Lanzamiento del Programa de Asociados</span></p>
                                    <p><span>Lanzamiento de la plataforma virtual de la Asociación OMA.</span></p>
                                    <p><span>VI Programa Mujeres Roca.</span></p>
                                    <p><span>Tercer Congreso OMA</span> en el marco de Expomina 2022</p>
                                    <p><span>Primer Seminario OMA.</span></p>
                                    <p><span>20 talleres de coaching</span> de 2:30 horas de duración.</p>
                                    <p><span>19 laboratorios Virtuales y 5 presenciales de 2 días</p>
                                    <p><span>50 charlas </span>semanales enfocadas en habilidades blandas, innovación y fortalecimiento de valores.</p>
                                    <p><span>Realización de talleres en comunidades y empoderamiento en estudiantes universitarios.</span></p>
                                    <p><span>12 becas de la Escuela de Mentoring de España </span> con mentores internacionales</p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php include 'seccion_gracias_auspiciadores.html';?>

                <?php include 'seccion_alianzas.html';?>

                <?php include 'seccion_testimonios.html';?>
                
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>