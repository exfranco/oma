<!DOCTYPE html>
<html lang="es">
<head>
 	<meta charset="utf-8">
 	<title>Oma</title> 	
 	<?php include 'metas.html';?>
</head>
<body>
 	<div class="wrapper wrapper_interna"> 
	 	<header id="header">
 			<?php include 'header.html';?>
 		</header>
 		<div class="menu_mobile">
 			<?php include 'menu_mobile.html';?>
 		</div>
		<section class="seccion_principal">
			<div class="banner banner_especial"  style="background-image: url(images/banner-que-hacemos-testimonios.jpg);">				
                <div class="container">
                    <div class="box">
                        <h1>
                            AYÚDANOS A AYUDAR
                        </h1>
                        <div class="subtitulo">
                            NOS CONECTAMOS CON EL MUNDO INTERIOR DE NUESTROS JÓVENES DESDE <br>
                            SUS INFINITOS ROSTROS, SONRISAS Y BRILLO DE SUS OJOS
                        </div>
                        <div class="testimonio_detalle">
                            <div class="foto_marco">
                                <img src="images/testimonio-foto.png">
                            </div>
                            <div class="b">
                                <div class="texto">
                                    Ha sido una experiencia muy enriquecedora, completamente agradecida con OMA asimismo a mi mentor Iván Gonzales por su tiempo y compartir sus experiencias, las sesiones de mentor ingme ayudaron muchísimo en mi desarrollo personal y profesional ha sido un año de grandes aprendizajes. GRACIAS.
                                </div>
                                <div class="nombre">
                                    Anabel Sanchez Gamboa
                                </div>
                                <div class="cargo">
                                    Becaria de Escuela de Mentoring de España / Región La Libertad
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="btn_donaaqui">
                        <a href="dona-aqui.php" class="full"></a>
                        Dona Aquí
                    </div>
                </div>								
			</div>

            <section class="seccion_quehacemos">
                <div class="breadcrumb_caja">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a href="index.php">Inicio</a></li>
                            <li>/ <a href="que-hacemos-actividades.php">¿Qué hacemos?</a></li>
                            <li>/ <a href="que-hacemos-testimonios.php" class="activo">Testimonios</a></li>
                        </ul>
                    </div>                   											
                </div>

               
                <div class="container">
                    <div class="row">
                        
                        <div class="col-md-3 order-md-1">
                            <div class="btn_menu_lateral">
                                    Menu 
                                    <div class="sanguche">
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                        <div class="lines"></div>
                                    </div>
                                </div>
                            <ul class="menu_lateral">
                                <li>
                                    <a href="que-hacemos-actividades.php" class="full"></a>
                                    Actividades
                                </li>
                                <li>
                                    <a href="que-hacemos-programacion.php" class="full"></a>
                                    Programa 2022
                                </li>
                                <li class="activo">
                                    <a href="que-hacemos-testimonios.php" class="full"></a>
                                    Testimonios
                                </li>
                                <li>
                                    <a href="que-hacemos-logros.php" class="full"></a>
                                    Logros
                                </li>
                            </ul>
                        </div>
                        <div class="col-12">
                            <h2>
                            Testimonios
                            </h2>
                        </div>
                        <div class="col-md-9 order-md-12">
                            <ul class="testimonio_lista_detalle">
                                <li>                                        
                                    <div class="b">
                                        <div class="texto">
                                            <span class="orange medium">
                                                Laboratorio de coaching “Desarrollo integral de agentes de cambio” desarrollado por la Master 
                                                Coach Claudia Beltrán Ampuero director del Programa de Coaching y Mujeres Roca
                                            </span>
                                            Una experiencia de autodescubrimiento que me ha ido cautivando en cada sesión. No solo para mejorar en mi profesión, también me ha ayudado a encontrar respuestas para crecer. Acompañado de una excelente profesional y compañeros, he podido hacer un maravilloso proceso de transformación y cambio hasta llegar a lo que deseaba. Una experiencia de vida inolvidable. Gracias OMA y Amautas.
                                        </div>
                                        <div class="nombre">
                                        Joel Lidwer Chavez Cotrina 
                                        </div>
                                        <div class="cargo">
                                            Universidad Continental – Huancayo – Junín  <br>
                                            Ingeniería de Minas 

                                        </div>
                                    </div>
                                    <div class="foto">
                                        <img src="images/testimonio-lista-detalle-1.png">
                                    </div>
                                </li>

                                <li>                                        
                                    <div class="b">
                                        <div class="texto">
                                            <span>
                                                Laboratorio de coaching “Desarrollo integral de agentes de cambio” desarrollado por la Master 
                                                Coach Claudia Beltrán Ampuero director del Programa de Coaching y Mujeres Roca
                                            </span>
                                            De mi primer laboratorio de coaching virtual, me llevo la experiencia de que todo es posible, sobre todo las dinámicas virtuales, que he esperado vivirlas por bastante tiempo. Sobre la Coach Claudia Beltrán Ampuero, puedo comentar que ha sido una excelente coach y una gran persona, carismática y muy comprometida con su trabajo, por hacernos llegar todos los conocimientos aprendidos en estos dos días de aprendizaje. Es una alegría para mi compartir estos espacios de integración, de aprender cosas nuevas como agentes de cambio; en mucho de lo que se mencionó me iba identificando y sobre todo los hechos y pensamientos que yo he ido adquiriendo en mis años y por muchos acontecimientos y errores que yo he vivido, pero gracias a mis acciones he podido sobresalir y alcanzar las metas que tengo hoy en día. Ha sido de los momentos mas impactantes del laboratorio y me he sentido muy afortunado. “Gracias OMA”


                                        </div>
                                        <div class="nombre">
                                            Bruno Marcos Villanueva 
                                        </div>
                                        <div class="cargo">
                                            Presidente Nacional de Amautas Mineros 2021 - 2022 <br>
                                            Universidad Nacional Mayor de San Marcos  <br>
                                            Ingeniería Ambiental 
                                        </div>
                                    </div>
                                    <div class="foto">
                                        <img src="images/testimonio-lista-detalle-2.png">
                                    </div>
                                </li>
                                <li>                                        
                                    <div class="b">
                                        <div class="texto">
                                            <span>Taller de empleabilidad desarrollado por el Coach Nicolangelo Pelosi</span>
                                            Es muy importante saber que buscan las empresas actualmente de sus empleados. Asimismo, poder conocer y saber aplicar las herramientas necesarias para potenciar nuestras capacidades y ser mas empleables.  Gracias por taller brindado nos ayudan a tener un mejor panorama respecto al ámbito laboral. 
                                        </div>
                                        <div class="nombre">
                                            Laura Lucero Trujillo Carrascal
                                        </div>
                                        <div class="cargo">
                                            Universidad Nacional de Callao <br>
                                            Tesorera de Amautas Mineros Perú 2017 – 2018 <br>
                                            Ingeniería Química 

                                        </div>
                                    </div>
                                    <div class="foto">
                                        <img src="images/testimonio-lista-detalle-3.png">
                                    </div>
                                </li>
                                <li>                                        
                                    <div class="b">
                                        <div class="texto">
                                            <span>
                                                Laboratorio de coaching empleabilidad para los lideres Amautinos 23 y 24 de enero de 2021 
                                                diseñado y desarrollo por la Master Coach Claudia Beltrán Ampuero directora del Programa 
                                                de Coaching y Mujeres Roca. 
                                            </span>
                                            Ha sido una experiencia muy enriquecedora para mí. Estoy muy agradecida por el aporte de la Coach Claudia Beltrán y de mis compañeros, que al igual que yo, supimos aprovechar al máximo este taller. Pude identificar y a la vez potenciar diversos puntos clave en mi hoja de vida teniendo siempre como base el desarrollo de mis habilidades blandas. Sin duda alguna me llevo una gran experiencia, como las dinámicas grupales en las que tuvimos como reto la realización del video de convocatoria 2021, donde pude apreciar cuan importante es para mi el sector minero y su desarrollo. Muchas gracias OMA PERÚ, Coaching LAB y Amautas Mineros Perú que hicieron posible la realización de este evento.

                                        </div>
                                        <div class="nombre">
                                            Keymer Patricia Abril Sánchez 
                                        </div>
                                        <div class="cargo">
                                            Universidad Nacional de San Agustín 
                                            Presidenta Ejecutiva Regional de Amautas Mineros Arequipa 
                                        </div>
                                    </div>
                                    <div class="foto">
                                        <img src="images/testimonio-lista-detalle-4.png">
                                    </div>
                                </li>
                                <li>                                        
                                    <div class="b">
                                        <div class="texto">
                                            <span>
                                                Laboratorio de coaching Competencias para equipos de alto rendimiento, diseñado y 
                                                desarrollo por la Master Coach Claudia Beltrán Ampuero directora del Programa de 
                                                Coaching y Mujeres Roca.
                                            </span>
                                            Los laboratorios de coaching siempre son muy enriquecedores y este laboratorio en general estuvo lleno de aprendizajes que nos ayudan a mejorar nuestra comunicación y llegar a tener una comunicación productiva que complementa con una escucha activa, todo para un mejor trabajo en equipo, comenzando siempre por un autonocimiento personal y como equipo. Comenzando siempre por un autoconocimiento personal y como equipo. Es impresionante ver los puntos que no tomamos en cuenta, sin embargo, influyen en nuestro crecimiento, con estos laboratorios los podemos reconocer y aprender de ellos, buscando llegar a un liderazgo consciente que nos hará crecer también como asociación. Estoy muy agradecida con OMA de ser partícipe de este laboratorio que nos hace crecer como equipo, como profesionales y como personas. 

                                        </div>
                                        <div class="nombre">
                                            Sophia Urrunaga Torres 
                                        </div>
                                        <div class="cargo">
                                        Universidad Nacional Mayor de San Marcos <br>
                                        Tesorera de la Junto directiva Nacional de Amautas Mineros

                                        </div>
                                    </div>
                                    <div class="foto">
                                        <img src="images/testimonio-lista-detalle-5.png">
                                    </div>
                                </li>
                            </ul>
                        

                        </div>
                        
                    </div>
                </div>
                

                <?php include 'seccion_gracias_auspiciadores.html';?>
                <?php include 'seccion_alianzas.html';?>

                <div class="seccion_actividades_inferior">
                    <div class="container">
                        <div class="titulo">
                            Actividades
                        </div>
                        <div class="lista_actividades">
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <article>
                                        <a href="#" class="full"></a>
                                        <div class="imagen">
                                            <img src="images/que-hacemos-actividades-1.jpg">
                                        </div>
                                        <div class="b">
                                            <div class="titulo">
                                                Programa Mujeres Roca
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            
                                <div class="col-sm-6 col-md-3">
                                    <article>
                                        <a href="#" class="full"></a>
                                        <div class="imagen">
                                            <img src="images/que-hacemos-actividades-2.jpg">
                                        </div>
                                        <div class="b">
                                            <div class="titulo">
                                                Programa de Coaching
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            
                                <div class="col-sm-6 col-md-3">
                                    <article>
                                        <a href="#" class="full"></a>
                                        <div class="imagen">
                                            <img src="images/que-hacemos-actividades-3.jpg">
                                        </div>
                                        <div class="b">
                                            <div class="titulo">
                                                Asociación OMA
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            
                                <div class="col-sm-6 col-md-3">
                                    <article>
                                        <a href="#" class="full"></a>
                                        <div class="imagen">
                                            <img src="images/que-hacemos-actividades-4.jpg">
                                        </div>
                                        <div class="b">
                                            <div class="titulo">
                                                Mentoring
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
           

		</section>
		 <footer>
			<?php include 'footer.html';?>
		</footer>
 	</div>

</body>
</html>